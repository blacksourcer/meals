const BASE_URL = '/api'

import { showError } from '../actions/app'
import { logout } from '../actions/auth'

const callApi = (endpoint, options = {}, authenticated = false) => {
    const token = localStorage.getItem('token')
    const config = authenticated && token ? {
            ...options,
            headers: {
                ...options.headers,
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/x-www-form-urlencoded; charset=UTF-8",
            }
        } : options


    return fetch(BASE_URL + endpoint, config)
        .then(response => {
            const contentType = response.headers.get("content-type")

            if (contentType && contentType.indexOf("application/json") !== -1) {
                return response.json().then(data => ({ response, data }))
            }

            return response.text().then(data => ({ response, data }))
        })
        .then(({ response, data }) => {
            if (!response.ok) {
                return Promise.reject({response, data})
            }

            return data
        })
}

export const CALL_API = 'Call API'

export default store => next => action => {
    const callAPI = action[CALL_API]

    if (typeof callAPI === 'undefined') {
        return next(action)
    }

    const { endpoint, options, events, authenticated } = callAPI
    const { onRequest, onResponse, onError } = events

    next(onRequest())

    return callApi(endpoint, options, authenticated)
        .then(
            data => next(onResponse(data)),
            ({response, data}) => response.status === 400 && onError
                ? Promise.reject(next(onError(response, data)))
                : (response.status === 401
                    ? Promise.reject(next(logout()))
                    : Promise.reject(next(showError(data || response.statusText)))
                )
        )
}