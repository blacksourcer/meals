<?php

namespace Calories\Controllers;

use Calories\Components\Authenticator;
use Calories\Controller;
use Calories\Models\Exceptions\ModelException;
use Calories\Models\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * Class AuthController
 *
 * @property \Doctrine\ORM\EntityManager $entityManager
 * @property Authenticator $authenticator
 *
 * @package Calories\Controllers
 */
class AuthController extends Controller
{
    /**
     * @param User $user
     * @return string
     */
    private function createToken(User $user): string
    {
        return $this->authenticator->createToken(
            new Authenticator\Identity(
                $user->getId(),
                $user->getName(),
                $user->getRole()
            )
        );
    }

    /**
     * @return \Phalcon\Http\ResponseInterface
     */
    public function login()
    {
        if (!$name = $this->getValueFromRequest('name')) {
            return $this->badRequestError("\"Name\" is required");
        }

        if (!$password = $this->getValueFromRequest('password')) {
            return $this->badRequestError("\"Password\" is required");
        }

        if (!$users = $this->entityManager
            ->getRepository('Calories\Models\User')
            ->findBy(['name' => $name])
        ) {
            return $this->badRequestError("\"Username\" and/or \"Password\" is invalid");
        }

        /**
         * @var User $user
         */
        list($user) = $users;

        if (!$user->verifyPassword($password)) {
            return $this->badRequestError("\"Username\" and/or \"Password\" is invalid");
        }

        return $this->json([
            'token' => $this->createToken($user),
        ]);
    }

    /**
     * @return \Phalcon\Http\ResponseInterface
     */
    public function signup()
    {
        $user = new User();

        if (null !== ($name = $this->getValueFromRequest('name'))) {
            $user->setName($name);
        }

        if (null !== ($password = $this->getValueFromRequest('password'))) {
            $user->setPassword($password);
        }

        try {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (ModelException $ex) {
            return $this->badRequestError($ex->getMessage());
        } catch (UniqueConstraintViolationException $ex) {
            return $this->badRequestError("User name is already taken");
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this->json([
            'token' => $this->createToken($user),
        ]);
    }
}