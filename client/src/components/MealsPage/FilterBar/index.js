import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import Dialog from 'material-ui/Dialog'
import DatePicker from 'material-ui/DatePicker'
import TimePicker from 'material-ui/TimePicker'
import RaisedButton from 'material-ui/RaisedButton'
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar'

import FlatButton from 'material-ui/FlatButton'

class FilterBar extends React.Component {

    constructor(props) {
        super(props)

        this.onApply = props.onApply

        this.state = {
            dateFrom: props.filter.dateFrom,
            dateTo: props.filter.dateTo,
            timeFrom: props.filter.timeFrom,
            timeTo: props.filter.timeTo,
            formOpen: false,
        }

        this.handleOpenForm = this.handleOpenForm.bind(this)
        this.handleCloseForm = this.handleCloseForm.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleOpenForm() {
        this.setState({
            formOpen: true
        })
    }

    handleCloseForm() {
        this.setState({
            formOpen: false
        })
    }

    handleChange(name, value) {
        this.setState({
            [name]: value
        })
    }

    handleSubmit(event) {
        event.preventDefault()

        this.onApply({
            dateFrom: this.state.dateFrom,
            dateTo: this.state.dateTo,
            timeFrom: this.state.timeFrom,
            timeTo: this.state.timeTo,
        })

        this.setState({
            formOpen: false
        })
    }

    render() {
        const filterDateText = this.state.dateFrom && this.state.dateTo
            ? "date: " + this.state.dateFrom.toLocaleDateString() + " to " + this.state.dateTo.toLocaleDateString()
            : (this.state.dateFrom
                ? "date from " + this.state.dateFrom.toLocaleDateString()
                : (this.state.dateTo
                    ? "date to " + this.state.dateTo.toLocaleDateString()
                    : null
                )
            )

        const filterTimeText = this.state.timeFrom && this.state.timeTo
            ? "time: " + this.state.timeFrom.toLocaleTimeString() + " to " + this.state.timeTo.toLocaleTimeString()
            : (this.state.timeFrom
                    ? "time from " + this.state.timeFrom.toLocaleTimeString()
                    : (this.state.timeTo
                            ? "time to " + this.state.timeTo.toLocaleTimeString()
                            : null
                    )
            )

        const filterText = filterDateText && filterTimeText
            ? filterDateText + ", " + filterTimeText
            : (filterDateText
                ? filterDateText
                : (filterTimeText ? filterTimeText : null)
            )

        return (
            <Toolbar>
                <ToolbarGroup>
                    <ToolbarTitle style={filterText ? {fontSize: '16px'} : {} }
                                  text={filterText ? "Filtered by " + filterText : "All time"}
                    />
                </ToolbarGroup>
                <ToolbarGroup>
                    <ToolbarSeparator />
                    <RaisedButton label="Filter"
                                  primary={true}
                                  disabled={!this.props.isEnabled}
                                  onClick={this.handleOpenForm}
                    />
                </ToolbarGroup>
                <Dialog
                    title="Set filter"
                    actions={[
                        <FlatButton label="Cancel" primary={true} onClick={this.handleCloseForm} />,
                        <FlatButton label="Apply" onClick={this.handleSubmit} />
                    ]}
                    modal={false}
                    open={this.state.formOpen}
                    onRequestClose={this.handleCloseForm}
                >
                    <DatePicker
                        floatingLabelText="Date from"
                        name="dateFrom"
                        value={this.state.dateFrom}
                        onChange={(e, value) => this.handleChange("dateFrom", value)}
                        onDismiss={(e, value) => this.handleChange("dateFrom", null)}
                        cancelLabel="Reset"
                        formatDate={date => date.toLocaleDateString()}
                        shouldDisableDate={date => this.state.dateTo && date > this.state.dateTo}
                        autoOk={true}
                    />
                    <DatePicker
                        floatingLabelText="Date to"
                        name="dateTo"
                        value={this.state.dateTo}
                        onChange={(e, value) => this.handleChange("dateTo", value)}
                        onDismiss={(e, value) => this.handleChange("dateTo", null)}
                        cancelLabel="Reset"
                        formatDate={date => date.toLocaleDateString()}
                        shouldDisableDate={date => this.state.dateFrom && date < this.state.dateFrom}
                        autoOk={true}
                    />
                    <TimePicker
                        name="timeFrom"
                        floatingLabelText="Time from"
                        value={this.state.timeFrom}
                        onChange={
                            (e, value) => this.handleChange(
                                "timeFrom",
                                this.state.timeTo && value > this.state.timeTo
                                    ? this.state.timeTo
                                    : moment(value).startOf('minute').toDate()
                            )
                        }
                        onDismiss={(e, value) => this.handleChange("timeFrom", null)}
                        cancelLabel="Reset"
                        format="24hr"
                        autoOk={true}
                    />
                    <TimePicker
                        name="timeTo"
                        floatingLabelText="Time to"
                        value={this.state.timeTo}
                        onChange={
                            (e, value) => this.handleChange(
                                "timeTo",
                                this.state.timeFrom && value < this.state.timeFrom
                                    ? this.state.timeFrom
                                    : moment(value).startOf('minute').seconds(59).toDate())
                        }
                        onDismiss={(e, value) => this.handleChange("timeTo", null)}
                        cancelLabel="Reset"
                        format="24hr"
                        autoOk={true}
                    />
                </Dialog>
            </Toolbar>
        )
    }
}

FilterBar.propTypes = {
    filter: PropTypes.shape({
        dateFrom: PropTypes.instanceOf(Date),
        dateTo: PropTypes.instanceOf(Date),
        timeFrom: PropTypes.instanceOf(Date),
        timeTo: PropTypes.instanceOf(Date),
    }),
    isEnabled: PropTypes.bool.isRequired,
    onApply: PropTypes.func.isRequired,
}

export default FilterBar