<?php

namespace Calories\Middleware;

use Phalcon\Mvc\Micro;
use Phalcon\Events\Event;
use Phalcon\Mvc\Micro\MiddlewareInterface;

/**
 * Class Accept
 *
 * Checks for 'Accept' header to equal supported MIME
 */
class Accept implements MiddlewareInterface
{
    protected static $supportedMime = [
        'application/json'
    ];

    /**
     * @param Event $event
     * @param Micro $application
     * @return bool
     */
    public function beforeHandleRoute(
        /** @noinspection PhpUnusedParameterInspection */
        Event $event,
        Micro $application
    ) {
        $accept = $application->request->getHeader('Accept');
        $supportedMime = array_merge(static::$supportedMime, ['*/*']);

        if ($accept && !in_array($accept, $supportedMime)) {
            $application
                ->response
                ->setStatusCode(415, "Unsupported media type")
                ->send();

            return false;
        }

        return true;
    }

    /**
     * Calls the middleware
     *
     * @param Micro $application
     *
     * @returns bool
     */
    public function call(Micro $application)
    {
        return true;
    }
}