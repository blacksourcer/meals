<?php

namespace Calories\Middleware;

use Calories\Components\Authenticator;
use Calories\Models\User;
use Phalcon\Mvc\Micro;
use Phalcon\Events\Event;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use Phalcon\Mvc\Router;

/**
 * Class Authorization
 *
 * Checks if user is authorized to use the resource
 *
 * @property Router $router
 *
 * @package Calories\Middleware
 */
class Authorization implements MiddlewareInterface
{
    /**
     * @var Authenticator
     */
    private $authenticator;

    /**
     * @var array
     */
    private $rules = [];

    /**
     * @var array
     */
    public static $rolesHierarchy = [
        User::ROLE_USER => 0,
        User::ROLE_MANAGER => 1,
        User::ROLE_ADMIN => 2
    ];

    /**
     * Authorization constructor
     *
     * Set rules as following:
     * [
     *      '/users' => User::ROLE_ADMIN,
     *      '/meals' => User::ROLE_USER
     * ]
     *
     * @param Authenticator $authenticator
     * @param array $rules
     */
    public function __construct(
        Authenticator $authenticator,
        array $rules = []
    ) {
        $this->authenticator = $authenticator;
        $this->rules = $rules;
    }

    /**
     * @param Event $event
     * @param Micro $application
     * @return bool
     */
    public function beforeExecuteRoute(
        /** @noinspection PhpUnusedParameterInspection */
        Event $event,
        Micro $application
    ) {
        if ($resource = $application->router->getRewriteUri()) {
            foreach ($this->rules as $pattern => $requiredRole) {
                if (preg_match($pattern, $resource)) {
                    if (!$identity = $this->authenticator->getCurrentIdentity()) {
                        $application
                            ->response
                            ->setStatusCode(401, "Unauthorized")
                            ->send();

                        return false;
                    }

                    $role = $identity->getRole();

                    if ((static::$rolesHierarchy[$role] ?? 0)
                        < (static::$rolesHierarchy[$requiredRole] ?? 0)
                    ) {
                        $application
                            ->response
                            ->setStatusCode(403, "Forbidden")
                            ->send();

                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Calls the middleware
     *
     * @param Micro $application
     *
     * @returns bool
     */
    public function call(Micro $application)
    {
        return true;
    }
}