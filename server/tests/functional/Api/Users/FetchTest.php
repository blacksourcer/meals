<?php

namespace Calories\Tests\Functional\Api\Users;

use Calories\Models\User;
use Calories\Tests\Functional\Api\MealsTest;

/**
 * Class FetchTest
 *
 * @package Calories\Tests\Functional\Api\Users
 */
class FetchTest extends MealsTest
{
    /**
     * When unsupported 'Accept' MIME requested it yields HTTP 415 Unsupported Media Type
     */
    public function testUnsupportedAcceptHeader()
    {
        $response = $this->curl->get("{$this->baseUri}/api/users", [], [
            'Accept: application/xml'
        ]);

        $this->assertEquals(415, $response->header->statusCode);
    }

    /**
     * When no token passed in it yields HTTP 401 Unauthorized
     */
    public function testUnauthorized()
    {
        $response = $this->curl->get("{$this->baseUri}/api/users");

        $this->assertEquals(401, $response->header->statusCode);
    }

    /**
     * It yields HTTP 403 Forbidden when user is not ROLE_MANAGER or ROLE_ADMIN
     */
    public function testForbidden()
    {
        list(, $token) = $this->loginUser();

        list($statusCode) = $this->get("{$this->baseUri}/api/users", $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It shows users list
     */
    public function testGet()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $id1 = $this->createUser([
            'name' => $userName1 = "User_" . microtime(true),
            'password' => "qweqwe132",
        ], $token);

        $id2 = $this->createUser([
            'name' => $userName2 = "User_2_" . microtime(true),
            'password' => "qweqwe132",
        ], $token);

        list($statusCode, $headers, $body) = $this->get("{$this->baseUri}/api/users", $token);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        $users = array_values(array_filter(json_decode($body, true), function ($item) use ($userName1, $userName2){
            return $item['name'] === $userName1 || $item['name'] === $userName2;
        }));

        $this->assertEquals([
            [
                'id' => $id1,
                'name' => $userName1,
                'role' => User::ROLE_USER,
            ],
            [
                'id' => $id2,
                'name' => $userName2,
                'role' => User::ROLE_USER,
            ],
        ], $users);
    }

    /**
     * It yields HTTP 403 Forbidden when user is not ROLE_MANAGER or ROLE_ADMIN
     */
    public function testViewForbidden()
    {
        list(, $token) = $this->loginUser();

        list($statusCode) = $this->get("{$this->baseUri}/api/users/0", $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It shows user record
     */
    public function testView()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $id = $this->createUser([
            'name' => $userName = "User_" . microtime(true),
            'password' => "qweqwe132",
        ], $token);

        list(
            $statusCode,
            $headers,
            $body
        ) = $this->get("{$this->baseUri}/api/users/$id", $token);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        $this->assertEquals([
            'id' => $id,
            'name' => $userName,
            'role' => User::ROLE_USER,
        ], json_decode($body, true), '', 2);
    }
}