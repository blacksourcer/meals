<?php

namespace Calories\Models;

use Calories\Models\Exceptions\ModelException;

/**
 * Class Meal
 *
 * @package Calories\Models
 *
 * @Entity
 * @Table(name="meal", indexes={
 *     @Index(name="date_idx", columns={"date"}),
 *     @Index(name="date_time_idx", columns={"date", "time"})
 * })
 * @HasLifecycleCallbacks
 */
class Meal
{
    const NAME_MAX_LENGTH = 256;

    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var User
     *
     * @ManyToOne(targetEntity="Calories\Models\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;

    /**
     * @var string
     *
     * @Column(type="string", length=256, nullable=false)
     */
    protected $name;

    /**
     * @var \DateTime
     *
     * @Column(type="date", nullable=false)
     */
    protected $date;

    /**
     * @var int
     *
     * @Column(type="integer", nullable=false)
     */
    protected $time;

    /**
     * @var int
     *
     * @Column(type="integer", nullable=false)
     */
    protected $calories;

    /**
     * @throws ModelException
     */
    protected function validateName()
    {
        $name = $this->getName();

        if (null === $name) {
            throw new ModelException("\"Name\" can not be null");
        }

        if ($name === '') {
            throw new ModelException("\"Name\" can not be empty");
        }

        if (strlen($name) > self::NAME_MAX_LENGTH) {
            throw new ModelException("\"Name\" can not contain more than " . self::NAME_MAX_LENGTH . " characters");
        }
    }

    /**
     * @throws ModelException
     */
    protected function validateCalories()
    {
        $calories = $this->getCalories();

        if ($calories < 0) {
            throw new ModelException("\"Calories\" can not be negative");
        }

        if ($calories > INT_MAX) {
            throw new ModelException("\"Calories\" can not be larger than " . INT_MAX);
        }
    }

    /**
     * Meal constructor
     */
    public function __construct()
    {
        $this->setConsumed(new \DateTime("now"));
    }

    #region Getters and Setters

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser(?User $user)
    {
        $this->user = $user;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return static
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getConsumed(): ?\DateTime
    {
        if (!$this->date) {
            return null;
        }

        $dateTime = clone $this->date;

        if ($this->time) {
            $dateTime->add(new \DateInterval("PT{$this->time}S"));
        }

        return $dateTime;
    }

    /**
     * @param \DateTime|null $dateTime
     * @return static
     */
    public function setConsumed(?\DateTime $dateTime): self
    {
        if (null === $dateTime) {
            $this->date = null;
            $this->time = null;

            return $this;
        }

        $this->date = (clone $dateTime)->setTime(0, 0, 0);
        $this->time = $dateTime->getTimestamp() - $this->date->getTimestamp();

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCalories(): ?int
    {
        return $this->calories;
    }

    /**
     * @param int|null $calories
     * @return static
     */
    public function setCalories(?int $calories): self
    {
        $this->calories = $calories;

        return $this;
    }

    #endregion

    /**
     * @throws ModelException
     *
     * @PrePersist @PreUpdate
     */
    public function validate()
    {
        $this->validateName();
        $this->validateCalories();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'userId' => $this->getUser()->getId(),
            'name' => $this->getName(),
            'consumed' => $this->getConsumed()->format(\DateTime::ISO8601),
            'calories' => $this->getCalories(),
        ];
    }
}