<?php

namespace Calories\Tests\Functional\Api\Meals;

use Calories\Models\Meal;
use Calories\Models\User;
use Calories\Tests\Functional\Api\MealsTest;

/**
 * Class CreateTest
 *
 * @package Calories\Tests\Functional\Api\Meals
 */
class CreateTest extends MealsTest
{
    /**
     * When unsupported 'Accept' MIME requested it yields HTTP 415 Unsupported Media Type
     */
    public function testUnsupportedAcceptHeader()
    {
        $response = $this->curl->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'calories' => 500
        ], true, [
            'Accept: application/xml'
        ]);

        $this->assertEquals(415, $response->header->statusCode);
    }

    /**
     * When no token passed in it yields HTTP 401 Unauthorized
     */
    public function testUnauthorized()
    {
        $response = $this->curl->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'calories' => 500
        ]);

        $this->assertEquals(401, $response->header->statusCode);
    }

    /**
     * It yields HTTP 403 Forbidden when user tries to create a record with another userId
     */
    public function testForbidden()
    {
        list(, $token) = $this->loginUser();

        list($statusCode) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'calories' => 500,
            'userId' => 1,
        ], $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It creates record with just name and calories parameters provided
     * and sets date and time to current date and time
     */
    public function testCreate()
    {
        list(, $token) = $this->loginUser();

        list(
            $statusCode,
            $headers,
            $body
        ) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'calories' => 500
        ], $token);

        $this->assertEquals(201, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        $meal = json_decode($body, true);

        $this->assertEquals("{$this->baseUri}/api/meals/{$meal['id']}", $headers['Location']);

        $this->assertGreaterThan(0, $meal['id']);
        $this->assertEquals("Hamburger", $meal['name']);
        $this->assertEquals(time(), strtotime($meal['consumed']), '', 1);
        $this->assertEquals(500, $meal['calories']);

        $this->mealIdList[] = $meal['id'];
    }

    /**
     * It allows ADMIN to create record with specified userId
     */
    public function testCreateAdmin()
    {
        $userId = $this->createUser([], TOKEN_TESTING);

        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'calories' => 500,
            'userId' => $userId
        ], $token);

        $this->assertEquals(201, $statusCode);

        $meal = json_decode($body, true);

        $this->assertEquals($userId, $meal['userId']);

        $this->mealIdList[] = $meal['id'];
    }

    /**
     * It creates record with all parameters provided
     */
    public function testCreateExplicit()
    {
        list(, $token) = $this->loginUser();

        list(
            $statusCode,
            $headers,
            $body
        ) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'consumed' => "2001-12-31T23:59:59-0200",
            'time' => 3000,
            'calories' => 500
        ], $token);

        $this->assertEquals(201, $statusCode);

        $meal = json_decode($body, true);

        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);
        $this->assertEquals("{$this->baseUri}/api/meals/{$meal['id']}", $headers['Location']);

        $this->assertGreaterThan(0, $meal['id']);
        $this->assertEquals("Hamburger", $meal['name']);
        $this->assertEquals(strtotime("2002-01-01T01:59:59Z"), strtotime($meal['consumed']), '', 1);
        $this->assertEquals(500, $meal['calories']);

        $this->mealIdList[] = $meal['id'];
    }
    /**
     * It requires Name parameter to be passed
     */
    public function testCreateNoName()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'calories' => 500
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Name\" can not be null", json_decode($body));
    }

    /**
     * It requires Name parameter to be not empty
     */
    public function testCreateEmptyName()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'name' => '',
            'calories' => 500
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Name\" can not be empty", json_decode($body));
    }

    /**
     * It requires Name parameter to be shorter than 256 characters
     */
    public function testCreateNameTooLong()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
            ",
            'calories' => 500
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals(
            "\"Name\" can not contain more than " . Meal::NAME_MAX_LENGTH . " characters",
            json_decode($body)
        );
    }

    /**
     * It requires Consumed parameter to be not empty when it is passed
     */
    public function testCreateConsumedEmpty()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'consumed' => '',
            'calories' => 500
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Consumed\" parameter invalid", json_decode($body));
    }

    /**
     * It requires Consumed parameter to be a ISO 8601 string
     */
    public function testCreateConsumedIsInvalidString()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'consumed' => 'abcd',
            'calories' => 500
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Consumed\" parameter invalid", json_decode($body));
    }

    /**
     * It requires Consumed parameter to represent a valid date
     */
    public function testCreateConsumedIsInvalidDateZero()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'consumed' => '0000-00-00T00:00:00Z',
            'calories' => 500
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Consumed\" parameter invalid", json_decode($body));
    }

    /**
     * It requires Consumed parameter represent an existent date
     */
    public function testCreateConsumedIsInvalidNotExists()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'consumed' => '2017-02-29T00:00:00Z',
            'calories' => 500
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Consumed\" parameter invalid", json_decode($body));
    }

    /**
     * It doesn't support milliseconds in Consumed parameter
     */
    public function testCreateConsumedIsInvalidMilliseconds()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'consumed' => '2017-02-29T00:00:00.123Z',
            'calories' => 500
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Consumed\" parameter invalid", json_decode($body));
    }

    /**
     * It requires Calories parameter to be not empty
     */
    public function testCreateCaloriesIsNegative()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'calories' => -10,
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Calories\" can not be negative", json_decode($body));
    }

    /**
     * It requires Calories parameter to be less than 2^32
     */
    public function testCreateCaloriesIsTooBig()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/meals", [
            'name' => "Hamburger",
            'calories' => INT_MAX + 1,
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Calories\" can not be larger than " . INT_MAX, json_decode($body));
    }
}