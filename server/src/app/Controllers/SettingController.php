<?php

namespace Calories\Controllers;

use Calories\Components\Authenticator;
use Calories\Controller;
use Calories\Models\Exceptions\ModelException;
use Calories\Models\Setting;
use Calories\Models\User;
use Doctrine\DBAL\Types\Type;

/**
 * Class SettingController
 *
 * @property \Doctrine\ORM\EntityManager $entityManager
 * @property Authenticator $authenticator
 *
 * @package Calories\Controllers
 */
class SettingController extends Controller
{
    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function get()
    {
        if (!$identity = $this->authenticator->getCurrentIdentity()) {
            return $this->unauthorized();
        }

        try {
            $queryBuilder = $this->entityManager->createQueryBuilder();

            /**
             * @var Setting[] $settings
             */
            $settings = $queryBuilder
                ->select('p')
                ->from('Calories\Models\Setting', 'p')
                ->where($queryBuilder->expr()->eq('IDENTITY(p.user)', ':userId'))
                ->orderBy('p.name', 'ASC')
                ->setParameter(':userId', $identity->getId(), Type::INTEGER)
                ->getQuery()
                ->getResult();
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        $result = [];

        foreach ($settings as $setting) {
            $result[$setting->getName()] = $setting->getValue();
        }

        return $this->json((object)$result);
    }

    /**
     * @return \Phalcon\Http\ResponseInterface
     */
    public function save()
    {
        if (!$identity = $this->authenticator->getCurrentIdentity()) {
            return $this->unauthorized();
        }

        /**
         * @var User $user
         */
        if (!$user = $this->entityManager->find('Calories\Models\User', $identity->getId())) {
            return $this->serverError("Failed to read current user record");
        }

        try {
            $queryBuilder = $this->entityManager->createQueryBuilder();

            /**
             * @var Setting[] $settings
             */
            $settings = $queryBuilder
                ->select('p')
                ->from('Calories\Models\Setting', 'p')
                ->where($queryBuilder->expr()->eq('IDENTITY(p.user)', ':userId'))
                ->setParameter(':userId', $identity->getId(), Type::INTEGER)
                ->getQuery()
                ->getResult();

            $settingsMap = [];

            foreach ($settings as $setting) {
                $settingsMap[$setting->getName()] = $setting;
            }

            $settingsToSave = [];

            foreach ($this->getValuesFromRequest() as $name => $value) {
                $settingsToSave[] = $setting = $settingsMap[$name]
                    ?? (new Setting())->setName($name)->setUser($user);
                $setting->setValue($value);
            }
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        try {
            foreach ($settingsToSave as $setting) {
                $this->entityManager->persist($setting);
            }
            $this->entityManager->flush();
        } catch (ModelException $ex) {
            return $this->badRequestError($ex->getMessage());
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        $result = [];

        foreach ($settingsToSave as $setting) {
            $result[$setting->getName()] = $setting->getValue();
        }

        return $this->json($result);
    }
}