import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import RefreshIndicator from 'material-ui/RefreshIndicator'

import { ESTIMATED_CALORIES } from '../../actions/settings'

class SettingsDialog extends React.Component {

    constructor(props) {
        super(props)

        this.onSubmit = props.onSubmit
        this.onCancel = props.onCancel

        this.state = {
            [ESTIMATED_CALORIES]: props.items[ESTIMATED_CALORIES] || ''
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
    }

    handleChange(name, value) {
        this.setState({
            [name]: value
        })
    }

    handleSubmit() {
        this.onSubmit(this.state)
    }

    handleCancel() {
        this.onCancel(this.state)
    }

    render() {
        return (
            <Dialog
                title="User settings"
                actions={[
                    <FlatButton
                        label="Cancel"
                        primary={true}
                        onClick={this.handleCancel}
                        disabled={this.props.isWaiting}
                    />,
                    <FlatButton
                        label="Ok"
                        primary={true}
                        onClick={this.handleSubmit}
                        disabled={this.props.isWaiting}
                    />
                ]}
                modal={false}
                open={true}
                onRequestClose={this.handleCancel}
            >
                {this.props.error &&
                    <p style={{color: 'red'}}>{this.props.error}</p>
                }
                {this.props.isWaiting &&
                    <RefreshIndicator size={40}
                                      left={-20}
                                      top={100}
                                      status={'loading'}
                                      style={{ marginLeft: '50%' }}
                    />
                }
                <TextField
                    floatingLabelText="Estimated calories per day"
                    name={ESTIMATED_CALORIES}
                    value={this.state[ESTIMATED_CALORIES]}
                    onChange={(e, value) => this.handleChange(ESTIMATED_CALORIES, value)}
                    disabled={this.props.isWaiting}
                />
            </Dialog>
        )
    }
}

SettingsDialog.propTypes = {
    isWaiting: PropTypes.bool,
    error: PropTypes.string,
    items: PropTypes.shape({
        [ESTIMATED_CALORIES]: PropTypes.string,
    }).isRequired,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
}

export default SettingsDialog