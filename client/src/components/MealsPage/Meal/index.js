import React from 'react'
import PropTypes from 'prop-types'
import { ListItem } from 'material-ui/List'
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'
import MenuItem from 'material-ui/MenuItem'
import { grey400 } from 'material-ui/styles/colors'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import RefreshIndicator from 'material-ui/RefreshIndicator'

const Meal = ({ id, name, consumed, calories, isWaiting, onEditClick, onDeleteClick}) => {

    if (isWaiting) {
        return (
            <ListItem>
                <RefreshIndicator size={20}
                              left={-10}
                              top={5}
                              status={'loading'}
                              style={{ marginLeft: '50%' }} />
            </ListItem>
        )
    }

    return (
        <ListItem primaryText={name}
                     secondaryText={consumed.toLocaleString() + ', ' + calories + ' cal.'}
                     rightIconButton={
                         <IconMenu iconButtonElement={
                             <IconButton>
                                 <MoreVertIcon color={grey400} />
                             </IconButton>
                         }>
                             <MenuItem onClick={e => {
                                 e.preventDefault()
                                 onEditClick(id)
                             }}>Edit</MenuItem>
                             <MenuItem onClick={e => {
                                 e.preventDefault()
                                 onDeleteClick(id)
                             }}>Delete</MenuItem>
                         </IconMenu>
                     }
        />
    )
}

Meal.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    consumed: PropTypes.instanceOf(Date).isRequired,
    calories: PropTypes.number.isRequired,
    isWaiting: PropTypes.bool,
    onEditClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired,
}

export default Meal