<?php

namespace Calories\Controllers;

use Calories\Components\Authenticator;
use Calories\Controller;
use Calories\Models\Exceptions\ModelException;
use Calories\Models\Meal;
use Calories\Models\User;
use Doctrine\DBAL\Types\Type;

/**
 * Class MealController
 *
 * @property \Doctrine\ORM\EntityManager $entityManager
 * @property Authenticator $authenticator
 *
 * @package Calories\Controllers
 */
class MealController extends Controller
{

    /**
     * @param Authenticator\Identity $identity
     * @param $ownerUserId
     * @return bool
     */
    private function isAllowed(Authenticator\Identity $identity, $ownerUserId): bool
    {
        return $identity->getId() === $ownerUserId
            || $identity->getRole() === User::ROLE_ADMIN;
    }

    /**
     * @param int|null $userId
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function index(int $userId = null)
    {
        if (!$identity = $this->authenticator->getCurrentIdentity()) {
            return $this->unauthorized();
        }

        $userId = $userId ?? $identity->getId();

        if (!$this->isAllowed($identity, $userId)) {
            return $this->forbidden();
        }

        try {
            $dateFrom = $this->getDateTimeFromRequest('dateFrom', 'Y-m-d');
            $dateTo = $this->getDateTimeFromRequest('dateTo', 'Y-m-d');
            $timeFrom = $this->getDateTimeFromRequest('timeFrom', 'H:i:sO');
            $timeTo = $this->getDateTimeFromRequest('timeTo', 'H:i:sO');
        } catch (\InvalidArgumentException $ex) {
            return $this->badRequestError("Malformed request");
        }

        try {
            $queryBuilder = $this->entityManager->createQueryBuilder();

            $queryBuilder
                ->select('m')
                ->from('Calories\Models\Meal', 'm')
                ->where($queryBuilder->expr()->eq('IDENTITY(m.user)', ':userId'))
                ->orderBy('m.date, m.time', 'ASC')
                ->setParameter(':userId', $userId, Type::INTEGER);

            if (null !== $dateFrom) {
                $queryBuilder
                    ->andWhere('m.date >= :dateFrom')
                    ->setParameter(':dateFrom', $dateFrom, Type::DATE);
            }

            if (null !== $dateTo) {
                $queryBuilder
                    ->andWhere('m.date <= :dateTo')
                    ->setParameter(':dateTo', $dateTo, Type::DATE);
            }

            if (null !== $timeFrom) {
                $queryBuilder
                    ->andWhere('m.time >= :timeFrom')
                    ->setParameter(':timeFrom', $timeFrom->getTimestamp() - strtotime('today UTC'), Type::INTEGER);
            }

            if (null !== $timeTo) {
                $queryBuilder
                    ->andWhere('m.time <= :timeTo')
                    ->setParameter(':timeTo', $timeTo->getTimestamp() - strtotime('today UTC') , Type::INTEGER);
            }

            $result = $queryBuilder
                ->getQuery()
                ->getResult();
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this->json(array_map (function (Meal $meal) {
            return $meal->toArray();
        }, $result));
    }

    /**
     * @param int $id
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function view(int $id)
    {
        if (!$identity = $this->authenticator->getCurrentIdentity()) {
            return $this->unauthorized();
        }

        try {
            /**
             * @var Meal $meal
             */
            if (!$meal = $this->entityManager->find('Calories\Models\Meal', $id)) {
                return $this->notFoundError();
            }

            if (!$this->isAllowed($identity, $meal->getUser()->getId())) {
                return $this->forbidden();
            }
        }  catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this->json($meal->toArray());
    }

    /**
     * @return \Phalcon\Http\ResponseInterface
     */
    public function create()
    {
        if (!$identity = $this->authenticator->getCurrentIdentity()) {
            return $this->unauthorized();
        }

        $currentUserId = $identity->getId();

        $meal = new Meal();

        $userId = (int)$this->getValueFromRequest('userId') ?: $currentUserId;

        if ($userId !== $currentUserId
            && $identity->getRole() !== User::ROLE_ADMIN
        ) {
            return $this->forbidden();
        }

        /**
         * @var User $user
         */
        if (!$user = $this->entityManager->find('Calories\Models\User', $userId)) {
            return $this->serverError("Failed to find user record ID $userId");
        }

        $meal->setUser($user);

        if (null !== ($name = $this->getValueFromRequest('name'))) {
            $meal->setName($name);
        }

        try {
            if (null !== ($consumed = $this->getDateTimeFromRequest('consumed'))) {
                $meal->setConsumed($consumed);
            }
        } catch (\InvalidArgumentException $ex) {
            return $this->badRequestError("\"Consumed\" parameter invalid");
        }

        try {
            if (null !== ($calories = $this->getIntFromRequest('calories'))) {
                $meal->setCalories($calories);
            }
        } catch (\InvalidArgumentException $ex) {
            return $this->badRequestError("\"Calories\" parameter invalid");
        }

        try {
            $this->entityManager->persist($meal);
            $this->entityManager->flush();
        } catch (ModelException $ex) {
            return $this->badRequestError($ex->getMessage());
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this
            ->json($meal->toArray(), 201)
            ->setHeader("Location", "{$this->config->get('baseUri')}/api/meals/{$meal->getId()}");
    }

    /**
     * @param int $id
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function update(int $id)
    {
        if (!$identity = $this->authenticator->getCurrentIdentity()) {
            return $this->unauthorized();
        }

        try {
            if (!$meal = $this->entityManager->find('Calories\Models\Meal', $id)) {
                return $this->notFoundError();
            }
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        if (!$this->isAllowed($identity, $meal->getUser()->getId())) {
            return $this->forbidden();
        }

        if (null !== ($name = $this->getValueFromRequest('name'))) {
            $meal->setName($name);
        }

        try {
            if (null !== ($consumed = $this->getDateTimeFromRequest('consumed'))) {
                $meal->setConsumed($consumed);
            }
        } catch (\InvalidArgumentException $ex) {
            return $this->badRequestError("\"Consumed\" parameter invalid");
        }

        try {
            if (null !== ($calories = $this->getIntFromRequest('calories'))) {
                $meal->setCalories($calories);
            }
        } catch (\InvalidArgumentException $ex) {
            return $this->badRequestError("\"Calories\" parameter invalid");
        }

        try {
            $this->entityManager->persist($meal);
            $this->entityManager->flush();
        } catch (ModelException $ex) {
            return $this->badRequestError($ex->getMessage());
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this->json($meal->toArray());
    }

    /**
     * @param int $id
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function delete(int $id)
    {
        if (!$identity = $this->authenticator->getCurrentIdentity()) {
            return $this->unauthorized();
        }

        try {
            if (!$meal = $this->entityManager->find('Calories\Models\Meal', $id)) {
                return $this->notFoundError();
            }

            if (!$this->isAllowed($identity, $meal->getUser()->getId())) {
                return $this->forbidden();
            }

            $this->entityManager->remove($meal);
            $this->entityManager->flush();
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this->response;
    }
}