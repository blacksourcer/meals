import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import Checkbox from 'material-ui/Checkbox'
import MenuItem from 'material-ui/MenuItem'
import FlatButton from 'material-ui/FlatButton'
import RefreshIndicator from 'material-ui/RefreshIndicator'

import {
    ROLE_USER,
    ROLE_MANAGER,
    ROLE_ADMIN
} from "../../../actions/users"

class UserForm extends React.Component {
    constructor(props) {
        super(props)

        this.onSubmit = props.onSubmit
        this.onCancel = props.onCancel

        this.state = {
            id: props.id,
            name: props.name,
            password: props.password,
            role: props.role,
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
    }

    handleChange(name, value) {
        this.setState({
            [name]: value
        })
    }

    handleSubmit(event) {
        event.preventDefault()

        this.onSubmit(
            this.state.id,
            this.state.name,
            this.state.role,
            this.state.password
        )
    }

    handleCancel() {
        this.onCancel()
    }

    render() {
        return (
            <div>
                <Dialog
                    title={this.props.title || "Manage user record"}
                    actions={[
                        <FlatButton label="Cancel"
                                    onClick={this.handleCancel}
                                    disabled={this.props.isWaiting}
                        />,
                        <FlatButton label="Ok"
                                    primary={true}
                                    onClick={this.handleSubmit}
                                    disabled={this.props.isWaiting}
                        />
                    ]}
                    modal={true}
                    open={true}
                    onRequestClose={this.handleCancel}
                >
                    {this.props.error &&
                        <p style={{color: 'red'}}>{this.props.error}</p>
                    }
                    {this.props.isWaiting &&
                        <RefreshIndicator size={40}
                                          left={-20}
                                          top={100}
                                          status={'loading'}
                                          style={{ marginLeft: '50%' }}
                        />
                    }
                    <TextField
                        floatingLabelText="Name"
                        name="name"
                        value={this.state.name}
                        onChange={(e, value) => this.handleChange("name", value)}
                        disabled={this.props.isWaiting}
                    />
                    {this.props.auth.user.role === ROLE_ADMIN &&
                        <SelectField
                            floatingLabelText="Role"
                            value={this.state.role}
                            onChange={(e, index, value) => this.handleChange("role", value)}
                            disabled={this.props.isWaiting}
                        >
                            <MenuItem value={ROLE_USER} primaryText="User" />
                            <MenuItem value={ROLE_MANAGER} primaryText="Manager" />
                            <MenuItem value={ROLE_ADMIN} primaryText="Admin" />
                        </SelectField>
                    }
                    {this.state.password !== null &&
                        <TextField
                            floatingLabelText="Password"
                            name="password"
                            value={this.state.password}
                            onChange={(e, value) => this.handleChange("password", value)}
                            disabled={this.props.isWaiting}
                        />
                    }
                    {!this.props.isPasswordRequired &&
                        <Checkbox
                            label="Change password"
                            onCheck={(e, value) => this.handleChange("password", value ? '' : null)}
                            disabled={this.props.isWaiting}
                        />
                    }

                </Dialog>
            </div>
        )
    }
}

UserForm.propTypes = {
    auth: PropTypes.shape({
        user: PropTypes.shape({
            role: PropTypes.string.isRequired,
        }).isRequired,
    }).isRequired,
    id: PropTypes.number,
    name: PropTypes.string,
    role: PropTypes.string,
    isPasswordRequired: PropTypes.bool.isRequired,
    isWaiting: PropTypes.bool,
    title: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
}

UserForm.defaultProps = {
    name: '',
    password: null,
    role: ROLE_USER,
}

export default UserForm