import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'

class ErrorDialog extends React.Component {

    constructor(props) {
        super(props)

        this.onDismiss = props.onDismiss

        this.handleClose = this.handleClose.bind(this)
    }

    handleClose() {
        this.onDismiss()
    }

    render() {
        return (
            <div>
                <Dialog
                    title="Unexpected error occurred"
                    actions={[
                        <FlatButton label="Dismiss" primary={true} onClick={this.handleClose} />
                    ]}
                    modal={false}
                    open={true}
                    onRequestClose={this.handleClose}
                >
                    {this.props.message}
                </Dialog>
            </div>
        )
    }
}

ErrorDialog.propTypes = {
    error: PropTypes.string,
    onDismiss: PropTypes.func.isRequired,
}

export default ErrorDialog