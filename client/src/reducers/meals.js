import {
    SET_MEALS_FILTER,
    FETCH_MEALS_REQUEST,
    FETCH_MEALS_SUCCESS,
    CREATE_MEAL_BEGIN,
    CREATE_MEAL_CANCEL,
    CREATE_MEAL_REQUEST,
    CREATE_MEAL_SUCCESS,
    CREATE_MEAL_FAILURE,
    EDIT_MEAL_BEGIN,
    EDIT_MEAL_CANCEL,
    EDIT_MEAL_REQUEST,
    EDIT_MEAL_SUCCESS,
    EDIT_MEAL_FAILURE,
    DELETE_MEAL_REQUEST,
    DELETE_MEAL_SUCCESS,
} from '../actions/meals'

const initialState = {
    isFetching: false,
    changingItem: null,
    filter: {
        dateFrom: null,
        dateTo: null,
        timeFrom: null,
        timeTo: null,
    },
    items: [],
}

const meals = (state = initialState, action) => {
    switch (action.type) {
        case SET_MEALS_FILTER:
            return {...state, filter: action.filter}
        case FETCH_MEALS_REQUEST:
            return {...state, isFetching: true, changingItem: null}
        case FETCH_MEALS_SUCCESS:
            return {...state, isFetching: false, items: action.items }
        case CREATE_MEAL_BEGIN:
            return {...state, changingItem: {
                name: action.name,
                consumed: action.consumed,
                calories: action.calories,
            }}
        case CREATE_MEAL_CANCEL:
            return {...state, changingItem: null}
        case CREATE_MEAL_REQUEST:
            return {...state, changingItem: {
                ...state.changingItem,
                isWaiting: true,
            }}
        case CREATE_MEAL_SUCCESS:
            return {
                ...state,
                changingItem: null,
                items: [
                    ...state.items, {
                        id: action.id,
                        name: action.name,
                        consumed: action.consumed,
                        calories: action.calories,
                    }
                ]
            }
        case CREATE_MEAL_FAILURE:
            return {...state, changingItem: {
                ...state.changingItem,
                isWaiting: false,
                error: action.error
            }}

        case EDIT_MEAL_BEGIN:
            return {
                ...state,
                changingItem: state.items.find(item => item.id === action.id)
            }
        case EDIT_MEAL_CANCEL:
            return {...state, changingItem: null}
        case EDIT_MEAL_REQUEST:
            return {...state, changingItem: {
                ...state.changingItem,
                isWaiting: true,
            }}
        case EDIT_MEAL_SUCCESS:
            return {
                ...state,
                changingItem: null,
                items: state.items.map(
                    item => item.id === action.id
                        ? {
                            id: action.id,
                            name: action.name,
                            consumed: action.consumed,
                            calories: action.calories,
                        } : item
                )
            }
        case EDIT_MEAL_FAILURE:
            return {...state, changingItem: {
                ...state.changingItem,
                isWaiting: false,
                error: action.error
            }}
        case DELETE_MEAL_REQUEST:
            return {
                ...state,
                items: state.items.map(
                    item => item.id === action.id
                        ? {
                            ...item,
                            isWaiting: true
                        } : item
                )
            }
        case DELETE_MEAL_SUCCESS:
            return {
                ...state,
                items: state.items.filter(item => item.id !== action.id)
            }
        default:
            return state
    }
}

export default meals