<?php

namespace Calories\Models;

use Calories\Models\Exceptions\ModelException;

/**
 * Class User
 *
 * @package Calories\Models
 *
 * @Entity
 * @Table(name="user", uniqueConstraints={
 *     @UniqueConstraint(name="name_unq_idx",columns={"name"})
 * })
 * @HasLifecycleCallbacks
 */
class User
{
    const NAME_MAX_LENGTH = 64;
    const PASSWORD_MIN_LENGTH = 8;
    const PASSWORD_MAX_LENGTH = 64;

    const ROLE_USER = 'USER';
    const ROLE_MANAGER = 'MANAGER';
    const ROLE_ADMIN = 'ADMIN';

    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", length=64, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", length=8, nullable=false)
     */
    protected $role = self::ROLE_USER;

    /**
     * @var string
     *
     * @Column(type="string", length=255, nullable=false)
     */
    protected $password;

    /**
     * @throws ModelException
     */
    protected function validateName()
    {
        $name = $this->getName();

        if (null === $name) {
            throw new ModelException("\"Name\" can not be null");
        }

        if ($name === '') {
            throw new ModelException("\"Name\" can not be empty");
        }

        if (strlen($name) > self::NAME_MAX_LENGTH) {
            throw new ModelException("\"Name\" can not contain more than " . self::NAME_MAX_LENGTH . " characters");
        }
    }

    /**
     * @throws ModelException
     */
    protected function validateRole()
    {
        $role = $this->getRole();

        if (null === $role) {
            throw new ModelException("\"Role\" can not be null");
        }

        if (!in_array($role, [
            self::ROLE_USER,
            self::ROLE_MANAGER,
            self::ROLE_ADMIN
        ])) {
            throw new ModelException("\"Role\" is invalid");
        }
    }

    protected function validatePassword()
    {
        $password = $this->getPassword();

        if (null === $password) {
            throw new ModelException("\"Password\" can not be null");
        }

        if ($password === '') {
            throw new ModelException("\"Password\" can not be empty");
        }

        if (strlen($password) < self::PASSWORD_MIN_LENGTH
            || strlen($password) > self::PASSWORD_MAX_LENGTH
        ) {
            throw new ModelException(
                "\"Password\" should contain from " . self::PASSWORD_MIN_LENGTH . " to " . self::PASSWORD_MAX_LENGTH . " characters"
            );
        }
    }

    #region Getters and Setters

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return static
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param string|null $role
     * @return static
     */
    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return static
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    #endregion

    /**
     * @throws ModelException
     *
     * @PrePersist @PreUpdate
     */
    public function validate()
    {
        $this->validateName();
        $this->validateRole();
        $this->validatePassword();
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function hashPassword()
    {
        $this->setPassword(password_hash($this->getPassword(), PASSWORD_DEFAULT));
    }

    /**
     * @param $password
     * @return bool
     */
    public function verifyPassword($password): bool
    {
        return password_verify($password, $this->getPassword());
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'role' => $this->getRole(),
        ];
    }
}