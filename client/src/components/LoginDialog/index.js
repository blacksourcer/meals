import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import Checkbox from 'material-ui/Checkbox'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import RefreshIndicator from 'material-ui/RefreshIndicator'

class LoginDialog extends React.Component {

    constructor(props) {
        super(props)

        this.onSubmit = props.onSubmit

        this.state = {
            name: '',
            password: '',
            isNewAccount: false,
            isPasswordMasked: true
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(name, value) {
        this.setState({
            [name]: value
        })
    }

    handleSubmit(event) {
        event.preventDefault()

        this.onSubmit(
            this.state.name,
            this.state.password,
            this.state.isNewAccount,
        )
    }

    render() {
        return (
            <Dialog
                title="Log in or sign up"
                actions={[
                    <FlatButton
                        label="Ok"
                        primary={true}
                        onClick={this.handleSubmit}
                        disabled={this.props.isWaiting}
                    />
                ]}
                modal={true}
                open={true}
            >
                {this.props.error &&
                    <p style={{color: 'red'}}>{this.props.error}</p>
                }
                {this.props.isWaiting &&
                    <RefreshIndicator size={40}
                                      left={-20}
                                      top={100}
                                      status={'loading'}
                                      style={{ marginLeft: '50%' }}
                    />
                }
                <TextField
                    floatingLabelText="Login"
                    name="name"
                    value={this.state.name}
                    onChange={(e, value) => this.handleChange("name", value)}
                    disabled={this.props.isWaiting}
                /><br/>
                <TextField
                    floatingLabelText="Password"
                    name="password"
                    type={this.state.isPasswordMasked ? "password" : "text"}
                    value={this.state.password}
                    onChange={(e, value) => this.handleChange("password", value)}
                    disabled={this.props.isWaiting}
                /><br/>
                <Checkbox
                    label="Create new account"
                    onCheck={(e, value) => this.handleChange("isNewAccount", value)}
                    disabled={this.props.isWaiting}
                /><br/>
                <Checkbox
                    label="Show password"
                    checked={!this.state.isPasswordMasked}
                    onCheck={(e, value) => this.handleChange("isPasswordMasked", !value)}
                    disabled={this.props.isWaiting}
                />
            </Dialog>
        )
    }
}

LoginDialog.propTypes = {
    isWaiting: PropTypes.bool,
    error: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
}

export default LoginDialog