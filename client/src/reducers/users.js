import {
    FETCH_USERS_REQUEST,
    FETCH_USERS_SUCCESS,
    CREATE_USER_BEGIN,
    CREATE_USER_CANCEL,
    CREATE_USER_REQUEST,
    CREATE_USER_SUCCESS,
    CREATE_USER_FAILURE,
    EDIT_USER_BEGIN,
    EDIT_USER_CANCEL,
    EDIT_USER_REQUEST,
    EDIT_USER_SUCCESS,
    EDIT_USER_FAILURE,
    DELETE_USER_REQUEST,
    DELETE_USER_SUCCESS,
} from '../actions/users'

const initialState = {
    isFetching: false,
    changingItem: null,
    items: [],
}

const users = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USERS_REQUEST:
            return {...state, isFetching: true}
        case FETCH_USERS_SUCCESS:
            return { isFetching: false, items: action.items }
        case CREATE_USER_BEGIN:
            return {...state, changingItem: {
                name: action.name,
                role: action.role,
                password: action.password,
            }}
        case CREATE_USER_CANCEL:
            return {...state, changingItem: null}
        case CREATE_USER_REQUEST:
            return {...state, changingItem: {
                ...state.changingItem,
                isWaiting: true,
            }}
        case CREATE_USER_SUCCESS:
            return {
                ...state,
                changingItem: null,
                items: [
                    ...state.items, {
                        id: action.id,
                        name: action.name,
                        role: action.role,
                        password: action.password,
                    }
                ]
            }
        case CREATE_USER_FAILURE:
            return {...state, changingItem: {
                ...state.changingItem,
                isWaiting: false,
                error: action.error
            }}

        case EDIT_USER_BEGIN:
            return {
                ...state,
                changingItem: state.items.find(item => item.id === action.id)
            }
        case EDIT_USER_CANCEL:
            return {...state, changingItem: null}
        case EDIT_USER_REQUEST:
            return {...state, changingItem: {
                ...state.changingItem,
                isWaiting: true,
            }}
        case EDIT_USER_SUCCESS:
            return {
                ...state,
                changingItem: null,
                items: state.items.map(
                    item => item.id === action.id
                        ? {
                            id: action.id,
                            name: action.name,
                            role: action.role,
                        } : item
                )
            }
        case EDIT_USER_FAILURE:
            return {...state, changingItem: {
                ...state.changingItem,
                isWaiting: false,
                error: action.error
            }}
        case DELETE_USER_REQUEST:
            return {
                ...state,
                items: state.items.map(
                    item => item.id === action.id
                        ? {
                            ...item,
                            isWaiting: true
                        } : item
                )
            }
        case DELETE_USER_SUCCESS:
            return {
                ...state,
                items: state.items.filter(item => item.id !== action.id)
            }
        default:
            return state
    }
}

export default users