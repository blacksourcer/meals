import { connect } from 'react-redux'
import {
    setMealsFilter,
    fetchMeals,
    createMealBegin,
    createMealCancel,
    createMeal,
    editMealBegin,
    editMealCancel,
    editMeal,
    deleteMeal,
} from '../../actions/meals'

import MealsPage from '../../components/MealsPage'

const mapStateToProps = state => ({
    meals: state.meals,
    settings: state.settings,
    error: state.error,
})

const mapDispatchToProps = dispatch => ({
    onSetMealsFilter: (filter, userId) => {
        dispatch(setMealsFilter(filter))
        dispatch(fetchMeals(filter, userId)).catch(() => {})
    },
    onMealsFetch: (filter, userId) => {
        dispatch(fetchMeals(filter, userId)).catch(() => {})
    },
    onMealCreateBegin: () => {
        dispatch(createMealBegin('', new Date(), 0))
    },
    onMealCreateCancel: () => {
        dispatch(createMealCancel())
    },
    onMealCreateSubmit: (name, userId, consumed, calories) => {
        dispatch(createMeal(name, userId, consumed, calories))
            .catch(() => {})
    },
    onMealEditBegin: (id) => {
        dispatch(editMealBegin(id))
    },
    onMealEditCancel: () => {
        dispatch(editMealCancel())
    },
    onMealEditSubmit: (id, name, consumed, calories) => {
        dispatch(editMeal(id, name, consumed, calories))
            .catch(() => {})
    },
    onMealDelete: (id) => {
        dispatch(deleteMeal(id)).catch(() => {})
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(MealsPage)