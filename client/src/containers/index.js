import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    login,
    signup,
    logout
} from '../actions/auth'

import {
    fetchSettings,
    saveSettings,
    unloadSettings,
} from '../actions/settings'

import {
    showLoginDialog,
    closeLoginDialog,
    closeSettingsDialog,
    showSettingsDialog,
    dismissError
} from '../actions/app'

import App from '../components'

const mapStateToProps = state => ({
    app: state.app,
    auth: state.auth,
    settings: state.settings,
    error: state.error,
})

const mapDispatchToProps = dispatch => ({
    onLoginRequired: () => dispatch(showLoginDialog()),
    onSettingsRequired: () => dispatch(fetchSettings()).catch(() => {}),
    onLoginSubmit: (name, password, isNewAccount) => dispatch(
        isNewAccount ? signup(name, password) : login(name, password)
    )
        .then(
            () => dispatch(closeLoginDialog()),
            () => Promise.reject()
        )
        .then(
            () => dispatch(fetchSettings()),
            () => {}
        ),
    onSettingsClick: () => dispatch(showSettingsDialog()),
    onSettingsSubmit: (items) => dispatch(saveSettings(items))
        .then(
            () => dispatch(closeSettingsDialog()),
            () => {}
        ),
    onSettingsCancel: () => dispatch(closeSettingsDialog()),
    onLogout: () => {
        dispatch(logout())
        dispatch(unloadSettings())
    },
    onDismissError: () => dispatch(dismissError()),
})

export default connect(mapStateToProps, mapDispatchToProps)(App)