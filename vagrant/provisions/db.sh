#!/usr/bin/env bash

echo "Creating databases"

mysql -uroot -e "DROP DATABASE IF EXISTS calories"
mysql -uroot -e "CREATE DATABASE calories /*\!40100 DEFAULT CHARACTER SET utf8 */"

echo "Settings DB permissions"
mysql -uroot -e "GRANT ALL ON *.* TO 'vagrant'@'%' IDENTIFIED BY 'vagrant'; GRANT ALL ON *.* TO 'vagrant'@'localhost' IDENTIFIED BY 'vagrant'; FLUSH PRIVILEGES;"

echo "Done!"