<?php

namespace Calories\Tests\Unit\Models;

use Calories\Models\Setting;
use Calories\Tests\TestCase;

/**
 * Class PreferenceTest
 *
 * @package Calories\Tests\Unit\Models\User
 */
class PreferenceTest extends TestCase
{
    /**
     * Name should be of supported constants list
     * 
     * @expectedException \Calories\Models\Exceptions\ModelException
     */
    public function testNameIsEmpty()
    {
        (new Setting())
            ->setName("gibberish")
            ->validate();
    }

    /**
     * Value can not be set to string longer than permitted
     *
     * @expectedException \Calories\Models\Exceptions\ModelException
     */
    public function tesValueTooLong()
    {
        (new Setting())
            ->setName(Setting::ESTIMATED_CALORIES)
            ->setValue("
                0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
            ")
            ->validate();
    }
}
