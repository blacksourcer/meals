<?php

namespace Calories\Tests;

use Phalcon\Di;
use Phalcon\Test\UnitTestCase as PhalconTestCase;

/**
 * Class TestCase
 *
 * @package Calories\Tests
 */
abstract class TestCase extends PhalconTestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $di = require ROOT_DIR . '/di.php';

        Di::reset();

        Di::setDefault($di);

        $this->setDi($di);
    }
}