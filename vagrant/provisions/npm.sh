#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

if [ ! -f /etc/apt/sources.list.d/nodesource.list ]; then
    echo "Installing nodesource repository"
    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash - > /dev/null
fi

NODE_INSTALLED=`which node`

if [ ! $NODE_INSTALLED ]; then
    echo "Installing Node.js"
    apt-get -y install nodejs
fi

cd /vagrant/client

echo "Updating NPM packages"
sudo -H -u vagrant npm update --no-progress

echo "Building application"
sudo -H -u vagrant ./node_modules/.bin/webpack --no-color
