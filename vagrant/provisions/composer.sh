#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

COMPOSER_INSTALLED=`which composer`

if [ ! $COMPOSER_INSTALLED ]; then
    echo "Installing Composer"
    curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
fi

cd /vagrant/server

if [ ! -f /vagrant/server/composer.lock ]; then
    echo "Installing composer packages"
    sudo -H -u vagrant composer install --no-progress
else
    echo "Updating composer packages"
    sudo -H -u vagrant composer update --no-progress
fi