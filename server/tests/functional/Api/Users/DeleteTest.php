<?php

namespace Calories\Tests\Functional\Api\Users;

use Calories\Models\User;
use Calories\Tests\Functional\ApiTest;

/**
 * Class DeleteTest
 *
 * @package Calories\Tests\Functional\Api\Users
 */
class DeleteTest extends ApiTest
{
    /**
     * When unsupported 'Accept' MIME requested it yields HTTP 415 Unsupported Media Type
     */
    public function testUnsupportedAcceptHeader()
    {
        $response = $this->curl->delete("{$this->baseUri}/api/meals/0", [], [
            'Accept: application/xml'
        ]);

        $this->assertEquals(415, $response->header->statusCode);
    }

    /**
     * When no token passed in it yields HTTP 401 Unauthorized
     */
    public function testUnauthorized()
    {
        $response = $this->curl->delete("{$this->baseUri}/api/meals/0");

        $this->assertEquals(401, $response->header->statusCode);
    }

    /**
     * It yields HTTP 403 Forbidden when user don't have ROLE_ADMIN or ROLE_MANAGER privilege
     */
    public function testForbidden()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_USER]);

        list($statusCode) = $this->delete("{$this->baseUri}/api/users/0", $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It requires admin role to delete users with role higher then ROLE_USER
     */
    public function testRoleForbidden()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
            'role' => User::ROLE_MANAGER
        ], $token);

        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode) = $this->delete("{$this->baseUri}/api/users/$id", $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It responds with 404 code when record not found
     */
    public function testNotFound()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode) = $this->delete("{$this->baseUri}/api/users/0", $token);

        $this->assertEquals(404, $statusCode);
    }

    /**
     * It deletes the record
     */
    public function testDelete()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        list($statusCode) = $this->delete("{$this->baseUri}/api/users/$id", $token);

        $this->assertEquals(200, $statusCode);

        list($statusCode) = $this->get("{$this->baseUri}/api/users/$id", $token);

        $this->assertEquals(404, $statusCode);
    }
}