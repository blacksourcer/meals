import reducer from '../../src/reducers/users'
import {
    CREATE_USER_SUCCESS,
    EDIT_USER_BEGIN,
    EDIT_USER_SUCCESS,
    DELETE_USER_SUCCESS
} from '../../src/actions/users'

describe('Users reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            changingItem: null,
            isFetching: false,
            items: []
        })
    })

    it('should handle CREATE_USER_SUCCESS', () => {
        const state = {
            changingItem: {
                name: "User 1",
                role: "USER",
                password: "qweqwe123"
            },
            items: []
        }

        expect(reducer(state, {
            type: CREATE_USER_SUCCESS,
            id: 1,
            name: "User 1",
            role: "USER",
        })).toEqual({
            changingItem: null,
            items: [
                { id: 1, name: "User 1", role: "USER" }
            ]
        })
    })

    it('should handle EDIT_USER_BEGIN', () => {
        const state = {
            changingItem: null,
            items: [
                { id: 1, name: "User 1", role: "USER", },
                { id: 2, name: "User 2", role: "USER", },
            ]
        }

        expect(reducer(state, {
            type: EDIT_USER_BEGIN,
            id: 2
        })).toEqual({
            changingItem: { id: 2, name: "User 2", role: "USER", },
            items: [
                { id: 1, name: "User 1", role: "USER", },
                { id: 2, name: "User 2", role: "USER", },
            ]
        })
    })

    it('should handle EDIT_USER_SUCCESS', () => {
        const state = {
            changingItem: { id: 2, name: "User 2", role: "USER", },
            items: [
                { id: 1, name: "User 1", role: "USER", },
                { id: 2, name: "User 2", role: "USER", },
            ]
        }

        expect(reducer(state, {
            type: EDIT_USER_SUCCESS,
            id: 2,
            name: "New User 2",
            role: "MANAGER",
        })).toEqual({
            changingItem: null,
            items: [
                { id: 1, name: "User 1", role: "USER", },
                { id: 2, name: "New User 2", role: "MANAGER" },
            ]
        })
    })

    it('should handle DELETE_USER_SUCCESS', () => {
        const state = {
            changingItem: null,
            items: [
                { id: 1, name: "User 1", role: "USER", },
                { id: 2, name: "User 2", role: "USER", },
            ]
        }

        expect(reducer(state, {
            type: DELETE_USER_SUCCESS,
            id: 2,
        })).toEqual({
            changingItem: null,
            items: [
                { id: 1, name: "User 1", role: "USER", },
            ]
        })
    })
})