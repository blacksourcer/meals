import reducer from '../../src/reducers/auth'
import {
    AUTH_SUCCESS
} from '../../src/actions/auth'

describe('Auth reducer', () => {
    it('should return the initial state', () => {
        localStorage.setItem.mockReset()

        expect(reducer(undefined, {})).toEqual({
            isWaiting: false,
            user: null,
            error: null,
        })
        expect(localStorage.getItem).toHaveBeenLastCalledWith('token');
    })


    it('should handle AUTH_SUCCESS', () => {
        const state = {
            isWaiting: true,
            user: null,
        }

        expect(reducer(state, {
            type: AUTH_SUCCESS,
            id: 1,
            name: "User 1",
            role: "USER",
            token: "TEST TOKEN"
        })).toEqual({
            isWaiting: false,
            user: { id: 1, name: "User 1", role: "USER", token: "TEST TOKEN" }
        })
    })
})