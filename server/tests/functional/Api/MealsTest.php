<?php

namespace Calories\Tests\Functional\Api;

use Calories\Tests\Functional\ApiTest;

/**
 * Class MealsTest
 *
 * @package Calories\Tests\Services
 */
abstract class MealsTest extends ApiTest
{

    /**
     * @var int[]
     */
    protected $mealIdList = [];

    /**
     * Helper function to create a meal
     *
     * @param array $mealData
     * @param string $token
     * @return int
     */
    protected function createMeal(array $mealData, string $token)
    {
        $response = $this->curl->post("{$this->baseUri}/api/meals", $mealData, true, [
            "Authorization: Bearer $token"
        ]);

        $meal = json_decode($response->body, true);

        if (!$meal || !isset($meal['id'])) {
            $this->fail("Failed to create meal record with data: " . var_export($mealData, true));
        }

        return $this->mealIdList[] = $meal['id'];
    }

    public function tearDown()
    {
        parent::tearDown();

        foreach ($this->mealIdList as $mealId) {
            $this->curl->delete("{$this->baseUri}/api/meals/$mealId", [], [
                "Authorization: Bearer " . TOKEN_TESTING
            ]);
        }
    }
}