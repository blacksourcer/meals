import { urlEncodeParams } from '../utils'

import { CALL_API } from '../middleware/api'

export const FETCH_USERS_REQUEST = 'FETCH_USERS_REQUEST'
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS'

export const CREATE_USER_BEGIN = 'CREATE_USER_BEGIN'
export const CREATE_USER_CANCEL = 'CREATE_USER_CANCEL'
export const CREATE_USER_REQUEST = 'CREATE_USER_REQUEST'
export const CREATE_USER_SUCCESS = 'CREATE_USER_SUCCESS'
export const CREATE_USER_FAILURE = 'CREATE_USER_FAILURE'

export const EDIT_USER_BEGIN = 'EDIT_USER_BEGIN'
export const EDIT_USER_CANCEL = 'EDIT_USER_CANCEL'
export const EDIT_USER_REQUEST = 'EDIT_USER_REQUEST'
export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS'
export const EDIT_USER_FAILURE = 'EDIT_USER_FAILURE'

export const DELETE_USER_REQUEST = 'DELETE_USER_REQUEST'
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS'

export const ROLE_ADMIN = 'ADMIN'
export const ROLE_MANAGER = 'MANAGER'
export const ROLE_USER = 'USER'

const fetchUsersRequest = () => ({
    type: FETCH_USERS_REQUEST,
})

const fetchUsersSuccess = (items) => ({
    type: FETCH_USERS_SUCCESS,
    items
})

export const fetchUsers = () => ({
    [CALL_API]: {
        endpoint: "/users",
        events: {
            onRequest: () => dispatch => dispatch(fetchUsersRequest()),
            onResponse: (items) => dispatch => dispatch(fetchUsersSuccess(items)),
        },
        authenticated: true
    }
})

export const createUserBegin = (name, role, password) => ({
    type: CREATE_USER_BEGIN,
    name, 
    role, 
    password
})

export const createUserCancel = () => ({
    type: CREATE_USER_CANCEL
})

const createUserRequest = () => ({
    type: CREATE_USER_REQUEST,
})

const createUserSuccess = (id, name, role) => ({
    type: CREATE_USER_SUCCESS,
    id,
    name,
    role,
})

const createUserFailure = (error) => ({
    type: CREATE_USER_FAILURE,
    error
})

export const createUser = (name, role, password) => ({
    [CALL_API]: {
        endpoint: "/users",
        options: {
            method: 'post',
            body: urlEncodeParams({name, role, password}),
        },
        events: {
            onRequest: () => dispatch => dispatch(createUserRequest()),
            onResponse: (item) => dispatch => dispatch(
                createUserSuccess(item.id, item.name, item.role)
            ),
            onError: (response, message) => dispatch => dispatch(createUserFailure(message))
        },
        authenticated: true
    }
})

export const editUserBegin = (id) => ({
    type: EDIT_USER_BEGIN,
    id
})

export const editUserCancel = () => ({
    type: EDIT_USER_CANCEL
})

const editUserRequest = (id) => ({
    type: EDIT_USER_REQUEST,
    id
})

const editUserSuccess = (id, name, role) => ({
    type: EDIT_USER_SUCCESS,
    id,
    name,
    role,
})

const editUserFailure = (error) => ({
    type: EDIT_USER_FAILURE,
    error
})

export const editUser = (id, name, role, password) => ({
    [CALL_API]: {
        endpoint: "/users/" + id,
        options: {
            method: 'put',
            body: urlEncodeParams(
                password !== null
                    ? {name, role, password}
                    : {name, role}
            )
        },
        events: {
            onRequest: () => dispatch => dispatch(editUserRequest(id)),
            onResponse: (item) => dispatch => dispatch(
                editUserSuccess(item.id, item.name, item.role)
            ),
            onError: (response, message) => dispatch => dispatch(editUserFailure(message))
        },
        authenticated: true
    }
})

const deleteUserRequest = (id) => ({
    type: DELETE_USER_REQUEST,
    id
})

const deleteUserSuccess = (id) => ({
    type: DELETE_USER_SUCCESS,
    id
})

export const deleteUser = (id) => ({
    [CALL_API]: {
        endpoint: "/users/" + id,
        options: {
            method: 'delete',
        },
        events: {
            onRequest: () => dispatch => dispatch(deleteUserRequest(id)),
            onResponse: () => dispatch => dispatch(deleteUserSuccess(id))
        },
        authenticated: true
    }
})
