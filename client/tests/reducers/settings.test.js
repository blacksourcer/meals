import reducer from '../../src/reducers/settings'
import {
    ESTIMATED_CALORIES,
    SAVE_SETTINGS_SUCCESS,
} from '../../src/actions/settings'

describe('Settings reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            items: {
                [ESTIMATED_CALORIES]: '',
            },
            isFetching: false,
            isWaiting: false,
            error: null,
        })
    })

    it('should handle SAVE_SETTINGS_SUCCESS', () => {
        const state = {
            items: {
                [ESTIMATED_CALORIES]: '',
            },
            isWaiting: true,
        }

        expect(reducer(state, { type: SAVE_SETTINGS_SUCCESS, items: {[ESTIMATED_CALORIES]: 10}}))
            .toEqual({
                items: {
                    [ESTIMATED_CALORIES]: 10,
                },
                isWaiting: false,
                error: null,
            })
    })
})