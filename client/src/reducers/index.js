import { combineReducers } from 'redux'
import auth from './auth'
import meals from './meals'
import users from './users'
import settings from './settings'
import app from './app'

const reducer = combineReducers({
    auth,
    meals,
    users,
    settings,
    app,
})

export default reducer