import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import api from '../../src/middleware/api'
import fetchMock from 'fetch-mock'

import {
    ESTIMATED_CALORIES,
    FETCH_SETTINGS_SUCCESS,
    FETCH_SETTINGS_REQUEST,
    SAVE_SETTINGS_REQUEST,
    SAVE_SETTINGS_SUCCESS,
    SAVE_SETTINGS_FAILURE,
    fetchSettings,
    saveSettings,
} from '../../src/actions/settings'

import { LOGOUT } from '../../src/actions/auth'
import { SHOW_ERROR } from '../../src/actions/app'
import {urlEncodeParams} from "../../src/utils/index";

const mockStore = configureMockStore([api, thunk ])

describe('Settings actions', () => {
    afterEach(() => {
        fetchMock.reset()
        fetchMock.restore()
        localStorage.clear()
        localStorage.setItem.mockReset()
    })

    it('fetches settings from the server and dispatches FETCH_SETTINGS_SUCCESS when succeeds', () => {
        const store = mockStore({ meals: [] })

        localStorage.__STORE__['token'] = 'TEST_TOKEN'

        fetchMock.getOnce('/api/settings', {
            body: { [ESTIMATED_CALORIES]: 1, },
            headers: { 'content-type': 'application/json' }
        }, {
            headers: { 'Authorization': 'Bearer TEST_TOKEN', }
        })

        return store.dispatch(fetchSettings())
            .then(() => {
                expect(store.getActions()).toEqual([
                    { type: FETCH_SETTINGS_REQUEST },
                    { type: FETCH_SETTINGS_SUCCESS, items: { [ESTIMATED_CALORIES]: 1, } }
                ])
            })
    })

    it('fetches settings from the server and dispatches SHOW_ERROR when fails', () => {
        const store = mockStore({ meals: [] })

        fetchMock.getOnce('/api/settings', {
            status: 500,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(fetchSettings())
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: FETCH_SETTINGS_REQUEST },
                    { type: SHOW_ERROR, error: "Internal Server Error"}
                ])
            })
    })

    it('saves settings and dispatches SAVE_SETTINGS_SUCCESS when succeeds', () => {
        localStorage.__STORE__['token'] = 'TEST_TOKEN'

        fetchMock.putOnce('/api/settings', {
            body: { [ESTIMATED_CALORIES]: 1 },
            headers: { 'content-type': 'application/json' }
        }, {
            headers: { 'Authorization': 'Bearer TEST_TOKEN', }
        })

        const store = mockStore({ meals: [] })

        return store.dispatch(saveSettings({ [ESTIMATED_CALORIES]: 1 }))
            .then(() => {
                expect(fetchMock.lastOptions().body).toEqual(urlEncodeParams(
                    { [ESTIMATED_CALORIES]: 1 }
                ))

                expect(store.getActions()).toEqual([
                    { type: SAVE_SETTINGS_REQUEST, },
                    { type: SAVE_SETTINGS_SUCCESS, items: {[ESTIMATED_CALORIES]: 1} },
                ])
            })
    })

    it('saves settings and dispatches SAVE_SETTINGS_FAILURE when fails (response status 400)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.putOnce('/api/settings', {
            status: 400,
            headers: { 'content-type': 'application/json' },
            body: "\"Value is invalid\"",
        })

        return store.dispatch(saveSettings({ [ESTIMATED_CALORIES]: 1 }))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: SAVE_SETTINGS_REQUEST, },
                    { type: SAVE_SETTINGS_FAILURE, error: "Value is invalid" },
                ])
            })
    })

    it('saves settings and dispatches LOGOUT when token has expired', () => {
        const store = mockStore({ meals: [] })

        fetchMock.putOnce('/api/settings', {
            status: 401,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(saveSettings({ [ESTIMATED_CALORIES]: 1 }))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: SAVE_SETTINGS_REQUEST, },
                    { type: LOGOUT }
                ])
            })
    })

    it('saves settings and dispatches SHOW_ERROR when fails (response status > 401)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.putOnce('/api/settings', {
            status: 500,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(saveSettings({ [ESTIMATED_CALORIES]: 1 }))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: SAVE_SETTINGS_REQUEST, },
                    { type: SHOW_ERROR, error: "Internal Server Error"}
                ])
            })
    })
})