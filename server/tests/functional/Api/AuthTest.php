<?php

namespace Calories\Tests\Functional\Api;

use Calories\Models\User;
use Calories\Tests\Functional\ApiTest;
use Firebase\JWT\JWT;
use Phalcon\Config;

/**
 * Class MealTest
 *
 * @package Calories\Tests\Services
 */
class AuthTest extends ApiTest
{
    /**
     * When unsupported 'Accept' MIME requested on signup it yields HTTP 415 Unsupported Media Type
     */
    public function testSignupUnsupportedAcceptHeader()
    {
        $response = $this->curl->post("{$this->baseUri}/api/users/signup", [
            'name' => "User_" . microtime(true),
            'password' => 'test1234',
        ], true, [
            'Accept: application/xml'
        ]);

        $this->assertEquals(415, $response->header->statusCode);
    }

    /**
     * It creates a user and returns auth token on sign-up
     */
    public function testSignup()
    {
        /**
         * @var Config $config
         */
        $config = $this->getConfig();

        $response = $this->curl->post("{$this->baseUri}/api/auth/signup", [
            'name' => $userName = "User_" . microtime(true),
            'password' => 'test1234',
        ]);

        $this->assertEquals(200, $response->header->statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $response->header->get('content-type'));

        $this->assertNotNull($json = json_decode($response->body, true));
        $this->assertNotNull($jwt = $json['token'] ?? null);

        $payload = JWT::decode($jwt, $config->get('jwt')->get('secret'), ['HS256']);

        $this->assertGreaterThan(0, $payload->id);
        $this->assertEquals($userName, $payload->name);
        $this->assertEquals(User::ROLE_USER, $payload->role);

        $this->userIdList[] = $payload->id;
    }

    /**
     * It returns user token when login and password are correct
     */
    public function testLogin()
    {
        /**
         * @var Config $config
         */
        $config = $this->getConfig();

        $this->createUser([
            'name' => $userName = "User_" . microtime(true),
            'password' => 'test1234',
        ], TOKEN_TESTING);

        $response = $this->curl->post("{$this->baseUri}/api/auth/login", [
            'name' => $userName,
            'password' => 'test1234',
        ]);

        $this->assertEquals(200, $response->header->statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $response->header->get('content-type'));

        $this->assertNotNull($json = json_decode($response->body, true));
        $this->assertNotNull($jwt = $json['token'] ?? null);

        $payload = JWT::decode($jwt, $config->get('jwt')->get('secret'), ['HS256']);

        $this->assertGreaterThan(0, $payload->id);
        $this->assertEquals($userName, $payload->name);
        $this->assertEquals(User::ROLE_USER, $payload->role);

        $this->userIdList[] = $payload->id;
    }

    /**
     * It returns 400 Bad Request when credentials are invalid
     */
    public function testLoginInvalidCredentials()
    {
        $response = $this->curl->post("{$this->baseUri}/api/auth/login", [
            'name' => "User_" . microtime(true),
            'password' => 'test1234',
        ]);

        $this->assertEquals(400, $response->header->statusCode);
        $this->assertEquals(
            "\"Username\" and/or \"Password\" is invalid",
            json_decode($response->body)
        );
    }
}