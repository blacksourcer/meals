import moment from 'moment'

export const urlEncodeParams = params => Object.keys(params).map((key) => {
    return params[key] !== null
        ? encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
        : null
}).filter(item => item !== null).join('&')

export const dateToServerDateFormat = date => moment(date).format("YYYY-MM-DD")
export const dateToServerTimeFormat = date => moment(date).format("HH:mm:ssZZ")
export const dateToServerFormat = date => moment(date).format()
