<?php

require_once __DIR__ . "/bootstrap.php";

/**
 * @var \Phalcon\DiInterface $di
 */
$di = require_once "di.php";

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet(
    $di->get('entityManager')
);
