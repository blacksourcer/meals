import jwt_decode from 'jwt-decode'

import {
    AUTH_REQUEST,
    AUTH_SUCCESS,
    AUTH_FAILURE,
    LOGOUT,
} from '../actions/auth'

const getCurrentUser = () => {
    const token = localStorage.getItem('token')
    const payload = token ? jwt_decode(token) : null
    const currentTs = Math.trunc((new Date()).getTime() / 1000) + 5

    return payload && payload.exp > currentTs ? jwt_decode(token) : null
}

const initialState = {
    isWaiting: false,
    user: getCurrentUser(),
    error: null,
}

const auth = (state = initialState, action) => {
    switch (action.type) {
        case AUTH_REQUEST:
            return { ...state, isWaiting: true }
        case AUTH_SUCCESS:
            return {
                ...state,
                isWaiting: false,
                user: {
                    id: action.id,
                    name: action.name,
                    role: action.role,
                    token: action.token,
                }
            }
        case AUTH_FAILURE:
            return {
                ...state,
                isWaiting: false,
                token: null,
                user: null,
                error: action.error,
            }
        case LOGOUT:
            return {
                ...state,
                user: null,
                error: null,
            }
        default:
            return state
    }
}

export default auth