export const SHOW_LOGIN_DIALOG = 'SHOW_LOGIN_DIALOG'
export const CLOSE_LOGIN_DIALOG = 'CLOSE_LOGIN_DIALOG'
export const SHOW_SETTINGS_DIALOG = 'SHOW_SETTINGS_DIALOG'
export const CLOSE_SETTINGS_DIALOG = 'CLOSE_SETTINGS_DIALOG'
export const SHOW_ERROR = 'SHOW_ERROR'
export const DISMISS_ERROR = 'DISMISS_ERROR'

export const showLoginDialog = () => ({
    type: SHOW_LOGIN_DIALOG
})

export const closeLoginDialog = () => ({
    type: CLOSE_LOGIN_DIALOG
})

export const showSettingsDialog = () => ({
    type: SHOW_SETTINGS_DIALOG,
})

export const closeSettingsDialog = () => ({
    type: CLOSE_SETTINGS_DIALOG,
})

export const showError = (error) => ({
    type: SHOW_ERROR,
    error
})

export const dismissError = () => ({
    type: DISMISS_ERROR
})