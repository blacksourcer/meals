import {
    ESTIMATED_CALORIES,
    FETCH_SETTINGS_REQUEST,
    FETCH_SETTINGS_SUCCESS,
    SAVE_SETTINGS_REQUEST,
    SAVE_SETTINGS_SUCCESS,
    SAVE_SETTINGS_FAILURE, UNLOAD_SETTINGS,
} from '../actions/settings'

const initialState = {
    items: {
        [ESTIMATED_CALORIES]: '',
    },
    isFetching: false,
    isWaiting: false,
    error: null,
}

const settings = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SETTINGS_REQUEST:
            return {...state, isFetching: true, isWaiting: false}
        case FETCH_SETTINGS_SUCCESS:
            return {...state, isFetching: false, items: action.items }
        case SAVE_SETTINGS_REQUEST:
            return {...state, isWaiting: true, }
        case SAVE_SETTINGS_SUCCESS:
            return {
                ...state,
                items: action.items,
                error: null,
                isWaiting: false,
            }
        case SAVE_SETTINGS_FAILURE:
            return {
                ...state,
                isWaiting: false,
                error: action.error
            }
        case UNLOAD_SETTINGS:
            return {
                ...state, items: {}
            }
        default:
            return state
    }
}

export default settings