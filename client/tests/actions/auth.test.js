import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import jwt from 'jwt-simple'

import api from '../../src/middleware/api'

import {
    AUTH_REQUEST,
    AUTH_SUCCESS,
    AUTH_FAILURE,
    login,
    signup,
    logout,
} from '../../src/actions/auth'

import { SHOW_ERROR } from '../../src/actions/app'

const mockStore = configureMockStore([api, thunk ])

describe('Auth actions', () => {
    afterEach(() => {
        fetchMock.reset()
        fetchMock.restore()
        localStorage.clear()
        localStorage.setItem.mockReset()
    })

    it('logs user in and dispatches AUTH_SUCCESS when succeeds', () => {
        const store = mockStore({ meals: [] })
        const token = jwt.encode({id: 1, name: "User", role: "USER"}, 'secret')

        fetchMock.postOnce('/api/auth/login', {
            body: {
                token,
            },
            headers: {'content-type': 'application/json'}
        })

        return store.dispatch(login('User', 'qweqwe123'))
            .then(() => {
                expect(localStorage.setItem).toHaveBeenLastCalledWith('token', token);

                expect(store.getActions()).toEqual([
                    { type: AUTH_REQUEST },
                    { type: AUTH_SUCCESS, id: 1, name: "User", role: "USER", token }
                ])
            })
    })

    it('logs user in and dispatches AUTH_FAILURE when fails (response status 400)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.postOnce('/api/auth/login', {
            status: 400,
            headers: { 'content-type': 'application/json' },
            body: "\"Name is invalid\"",
        })

        return store.dispatch(login('User', 'qweqwe123'))
            .catch(() => {
                expect(localStorage.setItem).toHaveBeenCalledTimes(0);

                expect(store.getActions()).toEqual([
                    { type: AUTH_REQUEST },
                    { type: AUTH_FAILURE, error: "Name is invalid" }
                ])
            })
    })

    it('logs user in and dispatches SHOW_ERROR when fails (response status > 400)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.postOnce('/api/auth/login', {
            status: 500,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(login('User', 'qweqwe123'))
            .catch(() => {
                expect(localStorage.setItem).toHaveBeenCalledTimes(0);

                expect(store.getActions()).toEqual([
                    { type: AUTH_REQUEST },
                    { type: SHOW_ERROR, error: "Internal Server Error"}
                ])
            })
    })

    it('signs user up and dispatches AUTH_SUCCESS when succeeds', () => {
        const store = mockStore({ meals: [] })
        const token = jwt.encode({id: 1, name: "User", role: "USER"}, 'secret')

        fetchMock.postOnce('/api/auth/signup', {
            body: {
                token,
            },
            headers: {'content-type': 'application/json'}
        })

        return store.dispatch(signup('User', 'qweqwe123'))
            .then(() => {
                expect(localStorage.setItem).toHaveBeenLastCalledWith('token', token);

                expect(store.getActions()).toEqual([
                    { type: AUTH_REQUEST },
                    { type: AUTH_SUCCESS, id: 1, name: "User", role: "USER", token }
                ])
            })
    })

    it('signs user up and dispatches AUTH_FAILURE when fails (response status 400)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.postOnce('/api/auth/signup', {
            status: 400,
            headers: { 'content-type': 'application/json' },
            body: "\"Name is invalid\"",
        })

        return store.dispatch(signup('User', 'qweqwe123'))
            .catch(() => {
                expect(localStorage.setItem).toHaveBeenCalledTimes(0);

                expect(store.getActions()).toEqual([
                    { type: AUTH_REQUEST },
                    { type: AUTH_FAILURE, error: "Name is invalid" }
                ])
            })
    })

    it('signs user up and dispatches SHOW_ERROR when fails (response status > 400)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.postOnce('/api/auth/signup', {
            status: 500,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(signup('User', 'qweqwe123'))
            .catch(() => {
                expect(localStorage.setItem).toHaveBeenCalledTimes(0);

                expect(store.getActions()).toEqual([
                    { type: AUTH_REQUEST },
                    { type: SHOW_ERROR, error: "Internal Server Error"}
                ])
            })
    })

    it('logs user out by deleting their token from localStorage', () => {
        const store = mockStore({ meals: [] })

        localStorage.setItem('token', 'TEST_TOKEN')

        store.dispatch(logout())

        expect(localStorage.removeItem).toHaveBeenLastCalledWith('token');
    })
})