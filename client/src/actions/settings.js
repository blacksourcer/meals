import { urlEncodeParams } from '../utils'

import { showError } from './app'

import { CALL_API } from '../middleware/api'

export const ESTIMATED_CALORIES = 'ESTIMATED_CALORIES'

export const FETCH_SETTINGS_REQUEST = 'FETCH_SETTINGS_REQUEST'
export const FETCH_SETTINGS_SUCCESS = 'FETCH_SETTINGS_SUCCESS'

export const SAVE_SETTINGS_REQUEST = 'SAVE_SETTINGS_REQUEST'
export const SAVE_SETTINGS_SUCCESS = 'SAVE_SETTINGS_SUCCESS'
export const SAVE_SETTINGS_FAILURE = 'SAVE_SETTINGS_FAILURE'

export const UNLOAD_SETTINGS = 'UNLOAD_SETTINGS'

const fetchSettingsRequest = () => ({
    type: FETCH_SETTINGS_REQUEST,
})

const fetchSettingsSuccess = (items) => ({
    type: FETCH_SETTINGS_SUCCESS,
    items
})

export const fetchSettings = () => ({
    [CALL_API]: {
        endpoint: "/settings",
        events: {
            onRequest: () => dispatch => dispatch(fetchSettingsRequest()),
            onResponse: (items) => dispatch => dispatch(fetchSettingsSuccess(items)),
            onError: (response, message) => dispatch => dispatch(showError(message || response.statusText))
        },
        authenticated: true
    }
})

const saveSettingsRequest = () => ({
    type: SAVE_SETTINGS_REQUEST,
})

const saveSettingsSuccess = (items) => ({
    type: SAVE_SETTINGS_SUCCESS,
    items,
})

const saveSettingsFailure = (error) => ({
    type: SAVE_SETTINGS_FAILURE,
    error,
})

export const saveSettings = (items) => ({
    [CALL_API]: {
        endpoint: "/settings",
        options: {
            method: 'put',
            body: urlEncodeParams(items),
        },
        events: {
            onRequest: () => dispatch => dispatch(saveSettingsRequest()),
            onResponse: (items) => dispatch => dispatch(saveSettingsSuccess(items)),
            onError: (response, message) => dispatch => dispatch(saveSettingsFailure(message))
        },
        authenticated: true
    }
})

export const unloadSettings = () => ({
    type: UNLOAD_SETTINGS,
})