import React from 'react'
import PropTypes from 'prop-types'

import User from '../User'

const UsersList = ({ auth, users, onUserEditClick, onUserDeleteClick }) => (
    <div>
        {users.items.map(user => (
            <User key={user.id} {...user}
                  auth={auth}
                  onEditClick = {onUserEditClick}
                  onDeleteClick = {onUserDeleteClick}/>
        ))}
    </div>
)

UsersList.propTypes = {
    auth: PropTypes.shape({
        user: PropTypes.shape({
            role: PropTypes.string.isRequired,
        }).isRequired,
    }).isRequired,
    users: PropTypes.shape({
        isFetching: PropTypes.bool.isRequired,
        items: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                role: PropTypes.string.isRequired,
                isEditing: PropTypes.bool,
                isWaiting: PropTypes.bool,
            }).isRequired
        )
    }),
    onUserEditClick: PropTypes.func.isRequired,
    onUserDeleteClick: PropTypes.func.isRequired
}

export default UsersList