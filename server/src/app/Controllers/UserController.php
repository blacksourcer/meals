<?php

namespace Calories\Controllers;

use Calories\Components\Authenticator;
use Calories\Controller;
use Calories\Models\Exceptions\ModelException;
use Calories\Models\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * Class UserController
 *
 * @property \Doctrine\ORM\EntityManager $entityManager
 * @property Authenticator $authenticator
 *
 * @package Calories\Controllers
 */
class UserController extends Controller
{
    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function index()
    {
        try {
            $users = $this->entityManager
                ->getRepository('Calories\Models\User')
                ->findBy([], ['id' => 'ASC']);
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this->json(array_map (function (User $meal) {
            return $meal->toArray();
        }, $users));
    }

    /**
     * @param int $id
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function view($id)
    {
        try {
            if (!$user = $this->entityManager->find('Calories\Models\User', $id)) {
                return $this->notFoundError();
            }
        }  catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this->json($user->toArray());
    }

    /**
     * @return \Phalcon\Http\ResponseInterface
     */
    public function create()
    {
        if (!$identity = $this->authenticator->getCurrentIdentity()) {
            return $this->unauthorized();
        }

        $user = new User();

        if (null !== ($name = $this->getValueFromRequest('name'))) {
            $user->setName($name);
        }

        if (null !== ($role = $this->getValueFromRequest('role'))) {
            if ($role !== User::ROLE_USER
                && $identity->getRole() !== User::ROLE_ADMIN
            ) {
                return $this->forbidden();
            }

            $user->setRole($role);
        }

        if (null !== ($password = $this->getValueFromRequest('password'))) {
            $user->setPassword($password);
        }

        try {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (ModelException $ex) {
            return $this->badRequestError($ex->getMessage());
        } catch (UniqueConstraintViolationException $ex) {
            return $this->badRequestError("\"Name\" is already taken");
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this
            ->json($user->toArray(), 201)
            ->setHeader("Location", "{$this->config->get('baseUri')}/api/users/{$user->getId()}");
    }

    /**
     * @param int $id
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function update(int $id)
    {
        if (!$identity = $this->authenticator->getCurrentIdentity()) {
            return $this->unauthorized();
        }

        try {
            if (!$user = $this->entityManager->find('Calories\Models\User', $id)) {
                return $this->notFoundError();
            }
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        if ($user->getRole() !== User::ROLE_USER
            && $identity->getRole() !== User::ROLE_ADMIN
        ) {
            return $this->forbidden();
        }

        if (null !== ($name = $this->getValueFromRequest('name'))) {
            $user->setName($name);
        }

        if (null !== ($role = $this->getValueFromRequest('role'))) {
            if ($role !== User::ROLE_USER
                && $identity->getRole() !== User::ROLE_ADMIN
            ) {
                return $this->forbidden();
            }

            $user->setRole($role);
        }

        if (null !== ($password = $this->getValueFromRequest('password'))) {
            $user->setPassword($password);
        }

        try {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (ModelException $ex) {
            return $this->badRequestError($ex->getMessage());
        } catch (UniqueConstraintViolationException $ex) {
            return $this->badRequestError("\"Name\" is already taken");
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this->json($user->toArray());
    }

    /**
     * @param int $id
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function delete($id)
    {
        if (!$identity = $this->authenticator->getCurrentIdentity()) {
            return $this->unauthorized();
        }

        try {
            if (!$user = $this->entityManager->find('Calories\Models\User', $id)) {
                return $this->notFoundError();
            }

            if ($user->getRole() !== User::ROLE_USER
                && $identity->getRole() !== User::ROLE_ADMIN
            ) {
                return $this->forbidden();
            }

            $this->entityManager->remove($user);
            $this->entityManager->flush();
        } catch (\Exception $ex) {
            return $this->serverError("Unexpected error");
        }

        return $this->response;
    }
}