<?php

namespace Calories\Tests\Functional\Api\Meals;

use Calories\Models\Meal;
use Calories\Models\User;
use Calories\Tests\Functional\Api\MealsTest;

/**
 * Class UpdateTest
 *
 * @package Calories\Tests\Functional\Api\Meals
 */
class UpdateTest extends MealsTest
{
    /**
     * When unsupported 'Accept' MIME requested it yields HTTP 415 Unsupported Media Type
     */
    public function testUnsupportedAcceptHeader()
    {
        $response = $this->curl->put("{$this->baseUri}/api/meals/0", [
            'name' => "Cheeseburger",
        ], true, [
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/xml',
        ]);

        $this->assertEquals(415, $response->header->statusCode);
    }

    /**
     * When no token passed in it yields HTTP 401 Unauthorized
     */
    public function testUnauthorized()
    {
        $response = $this->curl->put("{$this->baseUri}/api/meals/0", [
            'name' => "Hamburger",
            'calories' => 500
        ]);

        $this->assertEquals(401, $response->header->statusCode);
    }

    /**
     * It yields HTTP 403 Forbidden when user tries to update other user's meal
     */
    public function testForbidden()
    {
        list(, $token) = $this->loginUser();

        $mealId = $this->createMeal([
            'name' => "Hamburger",
            'calories' => 500,
        ], $token);

        list(, $token) = $this->loginUser();

        list($statusCode) = $this->put("{$this->baseUri}/api/meals/$mealId", [], $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It responds with 404 code when record not found
     */
    public function testNotFound()
    {
        list(, $token) = $this->loginUser();

        list($statusCode) = $this->put("{$this->baseUri}/api/meals/0", [
            'name' => "Cheeseburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 600
        ], $token);

        $this->assertEquals(404, $statusCode);
    }

    /**
     * It updates the record
     */
    public function testUpdate()
    {
        list($userId, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode, $headers) = $this->put("{$this->baseUri}/api/meals/$id", [
            'name' => "Cheeseburger",
            'consumed' => "2001-12-04T00:00:00+0000",
            'calories' => 600
        ], $token);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        list(, , $body) = $this->get("{$this->baseUri}/api/meals/$id", $token);

        $this->assertEquals([
            'id' => $id,
            'userId' => $userId,
            'name' => "Cheeseburger",
            'consumed' => "2001-12-04T00:00:00+0000",
            'calories' => 600,
        ], json_decode($body, true));
    }

    /**
     * It updates only passed properties
     */
    public function testUpdateProperties()
    {
        list($userId, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        $this->put("{$this->baseUri}/api/meals/$id", [
            'name' => "Cheeseburger",
        ], $token);

        list(, , $body) = $this->get("{$this->baseUri}/api/meals/$id", $token);

        $this->assertEquals([
            'id' => $id,
            'userId' => $userId,
            'name' => "Cheeseburger",
            'consumed' => "2001-12-03T00:00:00+0000",
            'calories' => 500,
        ], json_decode($body, true));

        $this->put("{$this->baseUri}/api/meals/$id", [
            'consumed' => "2001-12-04T00:00:00Z",
        ], $token);

        list(, , $body) = $this->get("{$this->baseUri}/api/meals/$id", $token);

        $this->assertEquals([
            'id' => $id,
            'userId' => $userId,
            'name' => "Cheeseburger",
            'consumed' => "2001-12-04T00:00:00+0000",
            'calories' => 500,
        ], json_decode($body, true));

        $this->put("{$this->baseUri}/api/meals/$id", [
            'calories' => 600,
        ], $token);

        list(, , $body) = $this->get("{$this->baseUri}/api/meals/$id", $token);

        $this->assertEquals([
            'id' => $id,
            'userId' => $userId,
            'name' => "Cheeseburger",
            'consumed' => "2001-12-04T00:00:00+0000",
            'calories' => 600,
        ], json_decode($body, true));
    }

    /**
     * It allows ADMIN to update any record
     */
    public function testUpdateAdmin()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        list($statusCode) = $this->put("{$this->baseUri}/api/meals/$id", [
            'calories' => 600
        ], $token);

        $this->assertEquals(200, $statusCode);
    }

    /**
     * It requires Name parameter to be not empty
     */
    public function testUpdateEmptyName()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/meals/$id", [
            'name' => '',
            'calories' => 500
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Name\" can not be empty", json_decode($body));
    }

    /**
     * It requires Name parameter to be shorter than 256 characters
     */
    public function testUpdateNameTooLong()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/meals/$id", [
            'name' => "
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
            ",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals(
            "\"Name\" can not contain more than " . Meal::NAME_MAX_LENGTH . " characters",
            json_decode($body)
        );
    }

    /**
     * It requires Consumed parameter to be not empty when it is passed
     */
    public function testUpdateConsumedEmpty()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/meals/$id", [
            'consumed' => '',
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Consumed\" parameter invalid", json_decode($body));
    }

    /**
     * It requires Consumed parameter to be a ISO 8601 string
     */
    public function testUpdateConsumedIsInvalidString()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/meals/$id", [
            'consumed' => 'abcd',
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Consumed\" parameter invalid", json_decode($body));
    }

    /**
     * It requires Consumed parameter to represent a valid date
     */
    public function testUpdateConsumedIsInvalidDateZero()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/meals/$id", [
            'consumed' => '0000-00-00T00:00:00Z',
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Consumed\" parameter invalid", json_decode($body));
    }

    /**
     * It requires Consumed parameter represent an existent date
     */
    public function testUpdateConsumedIsInvalidNotExists()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/meals/$id", [
            'consumed' => '2017-02-29T00:00:00Z',
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Consumed\" parameter invalid", json_decode($body));
    }

    /**
     * It doesn't support milliseconds in Consumed parameter
     */
    public function testUpdateConsumedIsInvalidMilliseconds()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/meals/$id", [
            'consumed' => '2017-02-29T00:00:00.123Z',
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Consumed\" parameter invalid", json_decode($body));
    }

    /**
     * It requires Calories parameter to be not empty
     */
    public function testUpdateCaloriesIsNegative()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/meals/$id", [
            'calories' => -10,
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Calories\" can not be negative", json_decode($body));
    }

    /**
     * It requires calories parameter to be less than 2^32
     */
    public function testUpdateCaloriesIsTooBig()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/meals/$id", [
            'calories' => INT_MAX + 1,
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Calories\" can not be larger than " . INT_MAX, json_decode($body));
    }
}