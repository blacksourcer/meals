<?php

return [
    'baseUri' => 'https://calories.com',
    'jwt' => [
        'secret' => '<PUT SECRET HERE>',
        'exp' => DAY_BY_SECONDS,
        'magicTokens' => [],
    ],
    'db' => [
        // PRODUCTION SETTINGS GO HERE
    ],
];
