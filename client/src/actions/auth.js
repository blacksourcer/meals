import jwt_decode from 'jwt-decode'
import { urlEncodeParams } from '../utils'

import { CALL_API } from '../middleware/api'

export const AUTH_REQUEST = 'AUTH_REQUEST'
export const AUTH_SUCCESS = 'AUTH_SUCCESS'
export const AUTH_FAILURE = 'AUTH_FAILURE'
export const LOGOUT = 'LOGOUT'

const authRequest = () => ({
    type: AUTH_REQUEST,
})

const authSuccess = (id, name, role, token) => {
    localStorage.setItem('token', token)

    return {
        type: AUTH_SUCCESS,
            id,
            name,
            role,
            token
    }
}

const authFailure = (error) => ({
    type: AUTH_FAILURE,
    error,
})

export const login = (name, password) => ({
    [CALL_API]: {
        endpoint: "/auth/login",
        options: {
            method: 'post',
            body: urlEncodeParams({name, password}),
        },
        events: {
            onRequest: () => dispatch => dispatch(authRequest(name, password)),
            onResponse: (item) => dispatch => {
                const { id, name, role } = jwt_decode(item.token)
                return dispatch(authSuccess(id, name, role, item.token))
            },
            onError: (response, message) => dispatch => dispatch(authFailure(message))
        }
    }
})

export const signup = (name, password) => ({
    [CALL_API]: {
        endpoint: "/auth/signup",
        options: {
            method: 'post',
            body: urlEncodeParams({name, password}),
        },
        events: {
            onRequest: () => dispatch => dispatch(authRequest(name, password)),
            onResponse: (item) => dispatch => {
                const { id, name, role } = jwt_decode(item.token)
                dispatch(authSuccess(id, name, role, item.token))
            },
            onError: (response, message) => dispatch => dispatch(authFailure(message))
        }
    }
})

export const logout = () => {
    localStorage.removeItem('token')
    return { type: LOGOUT }
}