<?php

use Phalcon\Events\Manager;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

use Calories\Middleware;

$eventsManager = new Manager();
$app = new Micro($di);

$meals = new MicroCollection();

$meals->setHandler('\Calories\Controllers\MealController', true);
$meals->setPrefix('/api/meals');
$meals->get('/', 'index');
$meals->get('/user/{userId:\d+}', 'index');
$meals->get('/{id:\d+}', 'view');
$meals->post('/', 'create');
$meals->put('/{id:\d+}', 'update');
$meals->delete('/{id:\d+}', 'delete');

$app->mount($meals);

$users = new MicroCollection();

$users->setHandler('\Calories\Controllers\UserController', true);
$users->setPrefix('/api/users');
$users->get('/', 'index');
$users->get('/{id:\d+}', 'view');
$users->post('/', 'create');
$users->put('/{id:\d+}', 'update');
$users->delete('/{id:\d+}', 'delete');

$app->mount($users);

$settings = new MicroCollection();

$settings->setHandler('\Calories\Controllers\SettingController', true);
$settings->setPrefix('/api/settings');
$settings->get('/', 'get');
$settings->put('/', 'save');

$app->mount($settings);

$auth = new MicroCollection();

$auth->setHandler('\Calories\Controllers\AuthController', true);
$auth->setPrefix('/api/auth');

$auth->post('/signup', 'signup');
$auth->post('/login', 'login');

$app->mount($auth);

$app->notFound(function () use ($di) {
    /**
     * @var \Phalcon\Http\ResponseInterface $response
     */
    $response = $di->get('response');

    return $response->setStatusCode(404, "Not found");
});

$eventsManager->attach('micro', new Middleware\Accept());
$eventsManager->attach('micro', new Middleware\Authorization(
    $di->get('authenticator'), [
        "/^\/api\/meals/" => \Calories\Models\User::ROLE_USER,
        "/^\/api\/settings/" => \Calories\Models\User::ROLE_USER,
        "/^\/api\/users/" => \Calories\Models\User::ROLE_MANAGER,
    ]
));

$app->setEventsManager($eventsManager);

return $app;
