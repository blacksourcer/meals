<?php

namespace Calories\Tests\Unit\Models;

use Calories\Models\User;

use Calories\Tests\TestCase;

/**
 * Class UserTest
 *
 * @package Calories\Tests\Services
 */
class UserTest extends TestCase
{
    /**
     * Name can not be empty
     * 
     * @expectedException \Calories\Models\Exceptions\ModelException
     */
    public function testNameIsEmpty()
    {
        (new User())
            ->setName("")
            ->setPassword("qweqwe123")
            ->setRole(User::ROLE_USER)
            ->validate();
    }

    /**
     * Name can not be set to string longer than permitted
     *
     * @expectedException \Calories\Models\Exceptions\ModelException
     */
    public function testNameTooLong()
    {
        (new User())
            ->setName("
                0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
            ")
            ->setPassword("qweqwe123")
            ->setRole(User::ROLE_USER)
            ->validate();
    }

    /**
     * Password should be longer than User::PASSWORD_MIN_LENGTH
     *
     * @expectedException \Calories\Models\Exceptions\ModelException
     */
    public function testPasswordTooShort()
    {
        (new User())
            ->setName("user")
            ->setPassword("1")
            ->setRole(User::ROLE_USER)
            ->validate();
    }

    /**
     * Password should not be longer than User::PASSWORD_MAX_LENGTH
     *
     * @expectedException \Calories\Models\Exceptions\ModelException
     */
    public function testPasswordTooLong()
    {
        (new User())
            ->setName("user")
            ->setPassword("
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            ")
            ->setRole(User::ROLE_USER)
            ->validate();
    }

    /**
     * Role should be equal to a predefined constant (User::ROLE_USER, etc.)
     *
     * @expectedException \Calories\Models\Exceptions\ModelException
     */
    public function testRoleInvalid()
    {
        (new User())
            ->setName("user")
            ->setPassword("qweqwe123")
            ->setRole("gibberish")
            ->validate();
    }

    /**
     * Correctly filled record should pass validation
     */
    public function testCorrectEntity()
    {
        (new User())
            ->setName("user")
            ->setPassword("qweqwe123")
            ->setRole(User::ROLE_USER)
            ->validate();

        $this->assertTrue(true);
    }

    /**
     *
     */
    public function testPasswordHash()
    {
        $user = (new User())
            ->setName("user")
            ->setPassword("qweqwe123")
            ->setRole(User::ROLE_USER);

        $this->assertEquals("qweqwe123", $user->getPassword());

        $user->hashPassword();

        $this->assertNotEquals("qweqwe123", $user->getPassword());
        $this->assertTrue($user->verifyPassword("qweqwe123"));
        $this->assertFalse($user->verifyPassword("qweqwe124"));
    }
}
