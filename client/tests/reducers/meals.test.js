import reducer from '../../src/reducers/meals'
import {
    CREATE_MEAL_SUCCESS,
    EDIT_MEAL_BEGIN,
    EDIT_MEAL_SUCCESS,
    DELETE_MEAL_SUCCESS
} from '../../src/actions/meals'

describe('Meals reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            filter: {
                dateFrom: null,
                dateTo: null,
                timeFrom: null,
                timeTo: null,
            },
            changingItem: null,
            isFetching: false,
            items: []
        })
    })

    it('should handle CREATE_MEAL_SUCCESS', () => {
        const state = {
            changingItem: {
                name: "Test meal",
                consumed: new Date('2017-11-19'),
                calories: 10
            },
            items: []
        }

        expect(reducer(state, {
            type: CREATE_MEAL_SUCCESS,
            id: 1,
            name: "Test meal",
            consumed: new Date('2017-11-19'),
            calories: 10
        })).toEqual({
            changingItem: null,
            items: [
                { id: 1, name: "Test meal", consumed: new Date('2017-11-19'), calories: 10 }
            ]
        })
    })

    it('should handle EDIT_MEAL_BEGIN', () => {
        const state = {
            changingItem: null,
            items: [
                { id: 1, name: "Test 1", consumed: new Date('2017-11-19'), calories: 10 },
                { id: 2, name: "Test 2", consumed: new Date('2017-11-19'), calories: 10 }
            ]
        }

        expect(reducer(state, {
            type: EDIT_MEAL_BEGIN,
            id: 2
        })).toEqual({
            changingItem: { id: 2, name: "Test 2", consumed: new Date('2017-11-19'), calories: 10 },
            items: [
                { id: 1, name: "Test 1", consumed: new Date('2017-11-19'), calories: 10 },
                { id: 2, name: "Test 2", consumed: new Date('2017-11-19'), calories: 10 }
            ]
        })
    })

    it('should handle EDIT_MEAL_SUCCESS', () => {
        const state = {
            changingItem: { id: 2, name: "Test 2", consumed: new Date('2017-11-19'), calories: 10 },
            items: [
                { id: 1, name: "Test 1", consumed: new Date('2017-11-19'), calories: 10 },
                { id: 2, name: "Test 2", consumed: new Date('2017-11-19'), calories: 10 }
            ]
        }

        expect(reducer(state, {
            type: EDIT_MEAL_SUCCESS,
            id: 2,
            name: "New Test 2",
            consumed: new Date('2017-11-20'),
            calories: 30
        })).toEqual({
            changingItem: null,
            items: [
                { id: 1, name: "Test 1", consumed: new Date('2017-11-19'), calories: 10 },
                { id: 2, name: "New Test 2", consumed: new Date('2017-11-20'), calories: 30 }
            ]
        })
    })

    it('should handle DELETE_MEAL_SUCCESS', () => {
        const state = {
            changingItem: null,
            items: [
                { id: 1, name: "Test 1", consumed: new Date('2017-11-19'), calories: 10 },
                { id: 2, name: "Test 2", consumed: new Date('2017-11-19'), calories: 10 }
            ]
        }

        expect(reducer(state, {
            type: DELETE_MEAL_SUCCESS,
            id: 2,
        })).toEqual({
            changingItem: null,
            items: [
                { id: 1, name: "Test 1", consumed: new Date('2017-11-19'), calories: 10 },
            ]
        })
    })
})