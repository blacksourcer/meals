<?php

return [
    'baseUri' => 'http://calories.local',
    'jwt' => [
        'secret' => 'vagrant',
        'exp' => DAY_BY_SECONDS,
        'magicTokens' => [
            TOKEN_TESTING => [
                'id' => 1,
                'name' => 'Test',
                'role' => \Calories\Models\User::ROLE_ADMIN,
            ]
        ]
    ],
    'db' => [
        'user' => 'vagrant',
        'password' => 'vagrant',
        'host' => 'localhost',
        'dbname' => 'calories',
    ],
];
