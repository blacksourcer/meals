<?php

namespace Calories\Tests\Functional\Api\Meals;

use Calories\Models\User;
use Calories\Tests\Functional\Api\MealsTest;

/**
 * Class DeleteTest
 *
 * @package Calories\Tests\Functional\Api\Meals
 */
class DeleteTest extends MealsTest
{
    /**
     * When unsupported 'Accept' MIME requested it yields HTTP 415 Unsupported Media Type
     */
    public function testUnsupportedAcceptHeader()
    {
        $response = $this->curl->delete("{$this->baseUri}/api/meals/0", [], [
            'Accept: application/xml'
        ]);

        $this->assertEquals(415, $response->header->statusCode);
    }

    /**
     * When no token passed in it yields HTTP 401 Unauthorized
     */
    public function testUnauthorized()
    {
        $response = $this->curl->delete("{$this->baseUri}/api/meals/0");

        $this->assertEquals(401, $response->header->statusCode);
    }

    /**
     * It yields HTTP 403 Forbidden when user tries to update other user's meal
     */
    public function testForbidden()
    {
        list(, $token) = $this->loginUser();

        $mealId = $this->createMeal([
            'name' => "Hamburger",
            'calories' => 500,
        ], $token);

        list(, $token) = $this->loginUser();

        list($statusCode) = $this->delete("{$this->baseUri}/api/meals/$mealId", $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It responds with 404 code when record not found
     */
    public function testNotFound()
    {
        list(, $token) = $this->loginUser();

        list($statusCode) = $this->delete("{$this->baseUri}/api/meals/0", $token);

        $this->assertEquals(404, $statusCode);
    }

    /**
     * It deletes the record
     */
    public function testDelete()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list($statusCode) = $this->delete("{$this->baseUri}/api/meals/$id", $token);

        $this->assertEquals(200, $statusCode);

        list($statusCode) = $this->get("{$this->baseUri}/api/meals/$id", $token);

        $this->assertEquals(404, $statusCode);
    }

    /**
     * It allows ADMIN to delete any record
     */
    public function testDeleteAdmin()
    {
        list(, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T00:00:00Z",
            'calories' => 500
        ], $token);

        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        list($statusCode) = $this->delete("{$this->baseUri}/api/meals/$id", $token);

        $this->assertEquals(200, $statusCode);
    }
}