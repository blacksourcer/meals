import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { ListItem } from 'material-ui/List'
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'
import MenuItem from 'material-ui/MenuItem'
import { grey400 } from 'material-ui/styles/colors'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import RefreshIndicator from 'material-ui/RefreshIndicator'
import {ROLE_ADMIN, ROLE_USER} from "../../../actions/users"

const User = ({ auth, id, name, role, isWaiting, onEditClick, onDeleteClick}) => {

    if (isWaiting) {
        return (
            <ListItem>
                <RefreshIndicator size={20}
                              left={-10}
                              top={5}
                              status={'loading'}
                              style={{ marginLeft: '50%' }} />
            </ListItem>
        )
    }

    return (
        <ListItem primaryText={name}
                  secondaryText={"Role: " + role}
                  rightIconButton={ (auth.user.role === ROLE_ADMIN || role === ROLE_USER) &&
                      <IconMenu iconButtonElement={
                          <IconButton>
                              <MoreVertIcon color={grey400}/>
                          </IconButton>
                      }>
                          {auth.user.role === ROLE_ADMIN &&
                              <MenuItem containerElement={
                                  <Link to={"/meals/user/" + id} />
                              }>Meals</MenuItem>
                          }
                          <MenuItem onClick={e => {
                              e.preventDefault()
                              onEditClick(id)
                          }}>Edit</MenuItem>
                          <MenuItem onClick={e => {
                              e.preventDefault()
                              onDeleteClick(id)
                          }}>Delete</MenuItem>
                      </IconMenu> || null
                  }
        />
    )
}

User.propTypes = {
    auth: PropTypes.shape({
        user: PropTypes.shape({
            role: PropTypes.string.isRequired,
        }).isRequired,
    }).isRequired,
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    isWaiting: PropTypes.bool,
    onEditClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired,
}

export default User