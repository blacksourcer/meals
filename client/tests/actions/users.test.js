import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import api from '../../src/middleware/api'
import fetchMock from 'fetch-mock'

import {
    FETCH_USERS_REQUEST,
    FETCH_USERS_SUCCESS,
    CREATE_USER_REQUEST,
    CREATE_USER_SUCCESS,
    CREATE_USER_FAILURE,
    EDIT_USER_REQUEST,
    EDIT_USER_SUCCESS,
    EDIT_USER_FAILURE,
    DELETE_USER_REQUEST,
    DELETE_USER_SUCCESS,
    fetchUsers,
    createUser,
    editUser,
    deleteUser
} from '../../src/actions/users'

import { LOGOUT } from '../../src/actions/auth'
import { SHOW_ERROR } from '../../src/actions/app'
import { urlEncodeParams } from '../../src/utils'

const mockStore = configureMockStore([api, thunk ])

describe('Users actions', () => {
    afterEach(() => {
        fetchMock.reset()
        fetchMock.restore()
        localStorage.clear()
        localStorage.setItem.mockReset()
    })

    it('fetches users from the server and dispatches FETCH_USERS_SUCCESS when succeeds', () => {
        const store = mockStore({ meals: [] })

        localStorage.__STORE__['token'] = 'TEST_TOKEN'

        fetchMock.getOnce('/api/users', {
            body: [
                {id: 1, name: "User 1", role: "User 1"},
                {id: 2, name: "User 2", role: "User 2"},
            ],
            headers: { 'content-type': 'application/json' }
        }, {
            headers: { 'Authorization': 'Bearer TEST_TOKEN', }
        })

        return store.dispatch(fetchUsers())
            .then(() => {
                expect(store.getActions()).toEqual([
                    { type: FETCH_USERS_REQUEST },
                    { type: FETCH_USERS_SUCCESS, items: [
                        {id: 1, name: "User 1", role: "User 1"},
                        {id: 2, name: "User 2", role: "User 2"},
                    ] }
                ])
            })
    })

    it('fetches users from the server and dispatches LOGOUT when token has expired', () => {
        const store = mockStore({ meals: [] })

        fetchMock.getOnce('/api/users', {
            status: 401,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(fetchUsers())
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: FETCH_USERS_REQUEST },
                    { type: LOGOUT }
                ])
            })
    })

    it('fetches users from the server and dispatches SHOW_ERROR when fails', () => {
        const store = mockStore({ meals: [] })

        fetchMock.getOnce('/api/users', {
            status: 500,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(fetchUsers())
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: FETCH_USERS_REQUEST },
                    { type: SHOW_ERROR, error: "Internal Server Error"}
                ])
            })
    })

    it('creates a record and dispatches CREATE_USER_SUCCESS when succeeds', () => {
        localStorage.__STORE__['token'] = 'TEST_TOKEN'

        fetchMock.postOnce('/api/users', {
            body: { id: 1, name: "User 1", role: "USER" },
            headers: { 'content-type': 'application/json' }
        }, {
            headers: { 'Authorization': 'Bearer TEST_TOKEN', }
        })

        const store = mockStore({ meals: [] })

        return store.dispatch(createUser("User 1", "USER", "qweqwe123")).then(() => {
            expect(fetchMock.lastOptions().body).toEqual(urlEncodeParams({
                name: "User 1",
                role: "USER",
                password: "qweqwe123",
            }))

            expect(store.getActions()).toEqual([
                { type: CREATE_USER_REQUEST, },
                { type: CREATE_USER_SUCCESS, id: 1, name: "User 1", role: "USER" },
            ])
        })
    })

    it('creates a record and dispatches CREATE_USER_FAILURE when fails (response status 400)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.postOnce('/api/users', {
            status: 400,
            headers: { 'content-type': 'application/json' },
            body: "\"Name is required\"",
        })

        return store.dispatch(createUser("", "USER", "qweqwe123"))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: CREATE_USER_REQUEST, },
                    { type: CREATE_USER_FAILURE, error: "Name is required" },
                ])
            })
    })

    it('creates a record and dispatches LOGOUT when token has expired', () => {
        const store = mockStore({ meals: [] })

        fetchMock.postOnce('/api/users', {
            status: 401,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(createUser("User 1", "USER", "qweqwe123"))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: CREATE_USER_REQUEST, },
                    { type: LOGOUT, }
                ])
            })
    })

    it('creates a record and dispatches SHOW_ERROR when fails (response status > 401)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.postOnce('/api/users', {
            status: 500,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(createUser("User 1", "USER", "qweqwe123"))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: CREATE_USER_REQUEST, },
                    { type: SHOW_ERROR, error: "Internal Server Error"}
                ])
            })
    })

    it('updates a record and dispatches EDIT_USER_SUCCESS when succeeds', () => {
        localStorage.__STORE__['token'] = 'TEST_TOKEN'

        fetchMock.putOnce('/api/users/1', {
            body: { id: 1, name: "User 1", role: "USER" },
            headers: { 'content-type': 'application/json' }
        }, {
            headers: { 'Authorization': 'Bearer TEST_TOKEN', }
        })

        const store = mockStore({ meals: [] })

        return store.dispatch(editUser(1, "User 1", "USER", "qweqwe123")).then(() => {
            expect(fetchMock.lastOptions().body).toEqual(urlEncodeParams({
                name: "User 1",
                role: "USER",
                password: "qweqwe123",
            }))

            expect(store.getActions()).toEqual([
                { type: EDIT_USER_REQUEST, id: 1 },
                { type: EDIT_USER_SUCCESS, id: 1, name: "User 1", role: "USER" },
            ])
        })
    })

    it('does not update password when it is set to null', () => {
        fetchMock.putOnce('/api/users/1', {
            body: { id: 1, name: "User 1", role: "USER" },
            headers: { 'content-type': 'application/json' }
        })

        const store = mockStore({ meals: [] })

        return store.dispatch(editUser(1, "User 1", "USER", null)).then(() => {
            expect(fetchMock.lastOptions().body).toEqual(urlEncodeParams({
                name: "User 1",
                role: "USER"
            }))

            expect(store.getActions()).toEqual([
                { type: EDIT_USER_REQUEST, id: 1 },
                { type: EDIT_USER_SUCCESS, id: 1, name: "User 1", role: "USER" },
            ])
        })
    })

    it('updates a record and dispatches EDIT_USER_FAILURE when fails (response status 400)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.putOnce('/api/users/1', {
            status: 400,
            headers: { 'content-type': 'application/json' },
            body: "\"Name is required\"",
        })

        return store.dispatch(editUser(1, "User 1", "USER", "qweqwe123"))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: EDIT_USER_REQUEST, id: 1 },
                    { type: EDIT_USER_FAILURE, error: "Name is required" },
                ])
            })
    })

    it('updates a record and dispatches LOGOUT when token has expired', () => {
        const store = mockStore({ meals: [] })

        fetchMock.putOnce('/api/users/1', {
            status: 401,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(editUser(1, "User 1", "USER", "qweqwe123"))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: EDIT_USER_REQUEST, id: 1 },
                    { type: LOGOUT },
                ])
            })
    })

    it('updates a record and dispatches SHOW_ERROR when fails (response status > 401)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.putOnce('/api/users/1', {
            status: 403,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(editUser(1, "User 1", "USER", "qweqwe123"))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: EDIT_USER_REQUEST, id: 1 },
                    { type: SHOW_ERROR, error: "Forbidden"},
                ])
            })
    })

    it('deletes a record, and dispatches DELETE_USER_SUCCESS when succeeds', () => {
        localStorage.__STORE__['token'] = 'TEST_TOKEN'

        fetchMock.deleteOnce('/api/users/1', { }, {
            headers: { 'Authorization': 'Bearer TEST_TOKEN', }
        })

        const store = mockStore({ meals: [] })

        return store.dispatch(deleteUser(1)).then(() => {
            expect(store.getActions()).toEqual([
                { type: DELETE_USER_REQUEST, id: 1 },
                { type: DELETE_USER_SUCCESS, id: 1 },
            ])
        })
    })

    it('deleted a record and dispatches LOGOUT when fails', () => {
        const store = mockStore({ meals: [] })

        fetchMock.deleteOnce('/api/users/1', {
            status: 401,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(deleteUser(1))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: DELETE_USER_REQUEST, id: 1 },
                    { type: LOGOUT },
                ])
            })
    })

    it('deleted a record and dispatches SHOW_ERROR when fails', () => {
        const store = mockStore({ meals: [] })

        fetchMock.deleteOnce('/api/users/1', {
            status: 403,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(deleteUser(1))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: DELETE_USER_REQUEST, id: 1 },
                    { type: SHOW_ERROR, error: "Forbidden" },
                ])
            })
    })
})