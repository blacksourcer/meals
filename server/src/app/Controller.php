<?php

namespace Calories;

use Phalcon\Config;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller as BaseController;

/**
 * Class Controller
 *
 * @property \Doctrine\ORM\EntityManager $entityManager
 * @property Config $config
 *
 * @package Calories
 */
class Controller extends BaseController
{
    /**
     * @param string $name
     * @return mixed|null
     */
    protected function getValueFromRequest(string $name)
    {
        if ($this->request->has($name)) {
            return $this->request->get($name);
        }
        if ($this->request->hasPut($name)) {
            return $this->request->getPut($name);
        }

        return null;
    }

    /**
     * @return mixed|null
     */
    protected function getValuesFromRequest(): array
    {
        return array_merge(
            $this->request->getPost(),
            $this->request->getPut()
        );
    }

    /**
     * @param string $name
     * @return int|null
     */
    protected function getIntFromRequest(string $name): ?int
    {
        if (null === ($value = $this->getValueFromRequest($name))) {
            return null;
        }

        if ($value === '') {
            throw new \InvalidArgumentException("Parameter is empty");
        }

        if (!is_numeric($value)) {
            throw new \InvalidArgumentException("Parameter is malformed");
        }

        return (int)$value;
    }

    /**
     * @param string $name
     * @param string $format
     * @return \DateTime|null
     */
    protected function getDateTimeFromRequest(
        string $name,
        string $format = \DateTime::ISO8601
    ): ?\DateTime {
        if (null === ($value = $this->getValueFromRequest($name))) {
            return null;
        }

        if ($value === '') {
            throw new \InvalidArgumentException("Parameter is empty");
        }

        if (!$dateTime = \DateTime::createFromFormat($format, $value)) {
            throw new \InvalidArgumentException("Parameter is malformed");
        }

        $errors = \DateTime::getLastErrors();

        if (!empty($errors['warning_count'])) {
            throw new \InvalidArgumentException("Parameter is incorrect");
        }

        return $dateTime->setTimezone(new \DateTimeZone('UTC'));
    }

    /**
     * @param mixed $data
     * @param int $statusCode
     * @param string $statusMessage
     * @return ResponseInterface
     */
    protected function json($data, int $statusCode = 200, string $statusMessage = 'OK'): ResponseInterface
    {
        return $this->response
            ->setStatusCode($statusCode, $statusMessage)
            ->setContentType('application/json', 'utf-8')
            ->setJsonContent($data);
    }

    /**
     * @param string|null $message
     * @return ResponseInterface
     */
    protected function badRequestError(string $message = null): ResponseInterface
    {
        $statusMessage = "Bad Request";

        return $this->json($message, 400, $statusMessage);
    }

    /**
     * @param string|null $message
     * @return ResponseInterface
     */
    protected function notFoundError(string $message = null): ResponseInterface
    {
        $statusMessage = "Not Found";

        return $this->json($message, 404, $statusMessage);
    }

    /**
     * @param string|null $message
     * @return ResponseInterface
     */
    protected function unauthorized(string $message = null): ResponseInterface
    {
        $statusMessage = "Unauthorized";

        return $this->json($message, 401, $statusMessage);
    }

    /**
     * @param string|null $message
     * @return ResponseInterface
     */
    protected function forbidden(string $message = null): ResponseInterface
    {
        $statusMessage = "Forbidden";

        return $this->json($message, 403, $statusMessage);
    }

    /**
     * @param string|null $message
     * @return ResponseInterface
     */
    protected function serverError(string $message = null): ResponseInterface
    {
        $statusMessage = "Internal Server Error";

        return $this->json($message, 500, $statusMessage);
    }
}