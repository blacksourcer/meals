<?php

namespace Calories\Tests\Functional\Api\Users;

use Calories\Models\User;
use Calories\Tests\Functional\ApiTest;

/**
 * Class UpdateTest
 *
 * @package Calories\Tests\Functional\Api\Users
 */
class UpdateTest extends ApiTest
{
    /**
     * When unsupported 'Accept' MIME requested it yields HTTP 415 Unsupported Media Type
     */
    public function testUnsupportedAcceptHeader()
    {
        $response = $this->curl->put("{$this->baseUri}/api/users/0", [
            'name' => "User_" . microtime(true),
        ], true, [
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/xml',
        ]);

        $this->assertEquals(415, $response->header->statusCode);
    }

    /**
     * When no token passed in it yields HTTP 401 Unauthorized
     */
    public function testUnauthorized()
    {
        $response = $this->curl->put("{$this->baseUri}/api/users/0", [
            'name' => "User_" . microtime(true),
            'calories' => 500
        ]);

        $this->assertEquals(401, $response->header->statusCode);
    }

    /**
     * It yields HTTP 403 Forbidden when user don't have ROLE_ADMIN or ROLE_MANAGER privilege
     */
    public function testForbidden()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_USER]);

        list($statusCode) = $this->put("{$this->baseUri}/api/users/0", [
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It responds with 404 code when record not found
     */
    public function testNotFound()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode) = $this->put("{$this->baseUri}/api/users/0", [
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        $this->assertEquals(404, $statusCode);
    }

    /**
     * It requires admin role to change users with role higher then ROLE_USER
     */
    public function testRoleForbidden()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
            'role' => User::ROLE_ADMIN
        ], $token);

        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode) = $this->put("{$this->baseUri}/api/users/$id", [
            'password' => "newqweqwe123",
        ], $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It requires admin role to change user's role
     */
    public function testRoleChangeForbidden()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        list($statusCode) = $this->put("{$this->baseUri}/api/users/$id", [
            'role' => User::ROLE_MANAGER,
        ], $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It updates the record
     */
    public function testUpdate()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $id = $this->createUser([
            'name' => $userName1 = "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        list(
            $statusCode,
            $headers
        ) = $this->put("{$this->baseUri}/api/users/$id", [
            'name' => $userName2 = "User_2_" . microtime(true),
            'password' => "qweqwe124",
        ], $token);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        list(, , $body) = $this->get("{$this->baseUri}/api/users/$id", $token);

        $this->assertEquals([
            'id' => $id,
            'name' => $userName2,
            'role' => User::ROLE_USER,
        ], json_decode($body, true));
    }

    /**
     * It allows ROLE_ADMIN to change other users' role
     */
    public function testUpdateAdmin()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        list($statusCode) = $this->put("{$this->baseUri}/api/users/$id", [
            'role' => User::ROLE_MANAGER
        ], $token);

        $this->assertEquals(200, $statusCode);

        list($statusCode) = $this->put("{$this->baseUri}/api/users/$id", [
            'role' => User::ROLE_ADMIN
        ], $token);

        $this->assertEquals(200, $statusCode);

        list($statusCode) = $this->put("{$this->baseUri}/api/users/$id", [
            'role' => User::ROLE_USER
        ], $token);

        $this->assertEquals(200, $statusCode);
    }

    /**
     * It requires Name parameter to be not empty
     */
    public function testUpdateEmptyName()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/users/$id", [
            'name' => '',
            'password' => "qweqwe123",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Name\" can not be empty", json_decode($body));
    }

    /**
     * It requires Name parameter to be shorter than 256 characters
     */
    public function testUpdateNameTooLong()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/users/$id", [
            'name' => "
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
            ",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals(
            "\"Name\" can not contain more than " . User::NAME_MAX_LENGTH . " characters",
            json_decode($body)
        );
    }

    /**
     * It doesn't allow to update users so they will get same names
     */
    public function testUpdateDuplicateName()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $this->createUser([
            'name' => $userName1 = "User_" . microtime(true)
        ], $token);

        $id = $this->createUser([
            'name' => $userName2 = "User_" . microtime(true)
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/users/$id", [
            'name' => $userName1,
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Name\" is already taken", json_decode($body));
    }

    /**
     * It requires Password parameter to be not empty when it is passed
     */
    public function testPasswordEmpty()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/users/$id", [
            'name' => "User_" . microtime(true),
            'password' => "",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Password\" can not be empty", json_decode($body));
    }

    /**
     * It requires Password parameter not to be shorter than 8 characters
     */
    public function testPasswordTooShort()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/users/$id", [
            'name' => "User_" . microtime(true),
            'password' => "12",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals(
            "\"Password\" should contain from " . User::PASSWORD_MIN_LENGTH . " to " . User::PASSWORD_MAX_LENGTH . " characters",
            json_decode($body)
        );
    }

    /**
     * It requires Password parameter to be not longer than 64 characters
     */
    public function testPasswordTooLong()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/users/$id", [
            'name' => "User_" . microtime(true),
            'password' => "
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            ",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals(
            "\"Password\" should contain from " . User::PASSWORD_MIN_LENGTH . " to " . User::PASSWORD_MAX_LENGTH . " characters",
            json_decode($body)
        );
    }

    /**
     * It requires role to be of (ROLE_USER, ROLE_MANAGER, ROLE_ADMIN)
     */
    public function testRoleInvalid()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        $id = $this->createUser([
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/users/$id", [
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
            'role' => "gibberish",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Role\" is invalid", json_decode($body));
    }
}