import {
    urlEncodeParams,
    dateToServerFormat,
    dateToServerDateFormat,
    dateToServerTimeFormat,
} from '../utils'

import { CALL_API } from '../middleware/api'

export const SET_MEALS_FILTER = 'SET_MEALS_FILTER'

export const FETCH_MEALS_REQUEST = 'FETCH_MEALS_REQUEST'
export const FETCH_MEALS_SUCCESS = 'FETCH_MEALS_SUCCESS'

export const CREATE_MEAL_BEGIN = 'CREATE_MEAL_BEGIN'
export const CREATE_MEAL_CANCEL = 'CREATE_MEAL_CANCEL'
export const CREATE_MEAL_REQUEST = 'CREATE_MEAL_REQUEST'
export const CREATE_MEAL_SUCCESS = 'CREATE_MEAL_SUCCESS'
export const CREATE_MEAL_FAILURE = 'CREATE_MEAL_FAILURE'

export const EDIT_MEAL_BEGIN = 'EDIT_MEAL_BEGIN'
export const EDIT_MEAL_CANCEL = 'EDIT_MEAL_CANCEL'
export const EDIT_MEAL_REQUEST = 'EDIT_MEAL_REQUEST'
export const EDIT_MEAL_SUCCESS = 'EDIT_MEAL_SUCCESS'
export const EDIT_MEAL_FAILURE = 'EDIT_MEAL_FAILURE'

export const DELETE_MEAL_REQUEST = 'DELETE_MEAL_REQUEST'
export const DELETE_MEAL_SUCCESS = 'DELETE_MEAL_SUCCESS'

export const setMealsFilter = (filter) => ({
    type: SET_MEALS_FILTER,
    filter
})

const fetchMealsRequest = () => ({
    type: FETCH_MEALS_REQUEST,
})

const fetchMealsSuccess = (items) => ({
    type: FETCH_MEALS_SUCCESS,
    items
})

export const fetchMeals = (filter, userId) => ({
    [CALL_API]: {
        endpoint: (userId ? "/meals/user/" + userId + "?" : "/meals?") + urlEncodeParams({
            dateFrom: filter.dateFrom ? dateToServerDateFormat(filter.dateFrom) : null,
            dateTo: filter.dateTo ? dateToServerDateFormat(filter.dateTo) : null,
            timeFrom: filter.timeFrom ? dateToServerTimeFormat(filter.timeFrom) : null,
            timeTo: filter.timeTo ? dateToServerTimeFormat(filter.timeTo) : null
        }),
        events: {
            onRequest: () => dispatch => dispatch(fetchMealsRequest()),
            onResponse: (json) => dispatch => dispatch(fetchMealsSuccess(json.map(item => ({
                ...item,
                consumed: new Date(item.consumed)
            })))),
        },
        authenticated: true
    }
})

export const createMealBegin = (name, consumed, calories) => ({
    type: CREATE_MEAL_BEGIN,
    name,
    consumed,
    calories
})

export const createMealCancel = () => ({
    type: CREATE_MEAL_CANCEL
})

const createMealRequest = () => ({
    type: CREATE_MEAL_REQUEST,
})

const createMealSuccess = (id, name, consumed, calories) => ({
    type: CREATE_MEAL_SUCCESS,
    id,
    name,
    consumed,
    calories
})

const createMealFailure = (error) => ({
    type: CREATE_MEAL_FAILURE,
    error
})

export const createMeal = (name, userId, consumed, calories) => ({
    [CALL_API]: {
        endpoint: "/meals",
        options: {
            method: 'post',
            body: urlEncodeParams({name, userId, consumed: dateToServerFormat(consumed), calories}),
        },
        events: {
            onRequest: () => dispatch => dispatch(createMealRequest()),
            onResponse: (item) => dispatch => dispatch(
                createMealSuccess(item.id, item.name, new Date(item.consumed), item.calories)
            ),
            onError: (response, message) => dispatch => dispatch(createMealFailure(message))
        },
        authenticated: true
    }
})

export const editMealBegin = (id) => ({
    type: EDIT_MEAL_BEGIN,
    id
})

export const editMealCancel = () => ({
    type: EDIT_MEAL_CANCEL
})

const editMealRequest = (id) => ({
    type: EDIT_MEAL_REQUEST,
    id
})

const editMealSuccess = (id, name, consumed, calories) => ({
    type: EDIT_MEAL_SUCCESS,
    id,
    name,
    consumed,
    calories
})

const editMealFailure = (error) => ({
    type: EDIT_MEAL_FAILURE,
    error
})

export const editMeal = (id, name, consumed, calories) => ({
    [CALL_API]: {
        endpoint: "/meals/" + id,
        options: {
            method: 'put',
            body: urlEncodeParams({name, consumed: dateToServerFormat(consumed), calories})
        },
        events: {
            onRequest: () => dispatch => dispatch(editMealRequest(id)),
            onResponse: (item) => dispatch => dispatch(
                editMealSuccess(item.id, item.name, new Date(item.consumed), item.calories)
            ),
            onError: (response, message) => dispatch => dispatch(editMealFailure(message))
        },
        authenticated: true
    }
})

const deleteMealRequest = (id) => ({
    type: DELETE_MEAL_REQUEST,
    id
})

const deleteMealSuccess = (id) => ({
    type: DELETE_MEAL_SUCCESS,
    id
})

export const deleteMeal = (id) => ({
    [CALL_API]: {
        endpoint: "/meals/" + id,
        options: {
            method: 'delete',
        },
        events: {
            onRequest: () => dispatch => dispatch(deleteMealRequest(id)),
            onResponse: () => dispatch => dispatch(
                deleteMealSuccess(id)
            )
        },
        authenticated: true
    }
})
