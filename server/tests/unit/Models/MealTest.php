<?php

namespace Calories\Tests\Unit\Models;

use Calories\Models\Meal;

use Calories\Tests\TestCase;

/**
 * Class MealTest
 *
 * @package Calories\Tests\Unit\Models
 */
class MealTest extends TestCase
{
    /**
     * Name can not be empty
     * 
     * @expectedException \Calories\Models\Exceptions\ModelException
     */
    public function testNameIsEmpty()
    {
        (new Meal())
            ->setName("")
            ->setConsumed(new \DateTime('2017-11-09'))
            ->setCalories(10)
            ->validate();
    }

    /**
     * Name can not be set to string longer than permitted
     *
     * @expectedException \Calories\Models\Exceptions\ModelException
     */
    public function testNameTooLong()
    {
        (new Meal())
            ->setName("
                0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
                0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
            ")
            ->setConsumed(new \DateTime('2017-11-09'))
            ->setCalories(10)
            ->validate();
    }

    /**
     * Number of calories in the meal can not be negative
     *
     * @expectedException \Calories\Models\Exceptions\ModelException
     */
    public function testCaloriesIsNegative()
    {
        (new Meal())
            ->setName("Hamburger")
            ->setConsumed(new \DateTime('2017-11-09'))
            ->setCalories(-1)
            ->validate();
    }

    /**
     * Correctly filled record should pass validation
     */
    public function testCorrectEntity()
    {
        (new Meal())
            ->setName("Hamburger")
            ->setConsumed(new \DateTime('2017-11-09'))
            ->setCalories(500)
            ->validate();

        $this->assertTrue(true);
    }
}
