import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import api from '../../src/middleware/api'
import fetchMock from 'fetch-mock'

import {
    FETCH_MEALS_REQUEST,
    FETCH_MEALS_SUCCESS,
    CREATE_MEAL_REQUEST,
    CREATE_MEAL_SUCCESS,
    CREATE_MEAL_FAILURE,
    EDIT_MEAL_REQUEST,
    EDIT_MEAL_SUCCESS,
    EDIT_MEAL_FAILURE,
    DELETE_MEAL_REQUEST,
    DELETE_MEAL_SUCCESS,
    fetchMeals,
    createMeal,
    editMeal,
    deleteMeal
} from '../../src/actions/meals'

import { LOGOUT } from '../../src/actions/auth'
import { SHOW_ERROR } from '../../src/actions/app'
import { urlEncodeParams } from '../../src/utils'

const mockStore = configureMockStore([api, thunk ])

describe('Meals actions', () => {
    afterEach(() => {
        fetchMock.reset()
        fetchMock.restore()
        localStorage.clear()
        localStorage.setItem.mockReset()
    })

    it('fetches records from the server and dispatches FETCH_MEALS_SUCCESS when succeeds', () => {
        const store = mockStore({ meals: [] })

        localStorage.__STORE__['token'] = 'TEST_TOKEN'

        fetchMock.getOnce('/api/meals?', {
            body: [
                {id: 1, name: "Test 1", consumed: "2001-12-04T00:00:00+0000", calories: 10},
                {id: 2, name: "Test 2", consumed: "2001-12-10T06:00:00+0200", calories: 20},
            ],
            headers: { 'content-type': 'application/json' }
        }, {
            headers: { 'Authorization': 'Bearer TEST_TOKEN', }
        })

        return store.dispatch(fetchMeals({
            dateFrom: null,
            dateTo: null,
            timeFrom: null,
            timeTo: null,
        }, null))
            .then(() => {
                expect(store.getActions()).toEqual([
                    { type: FETCH_MEALS_REQUEST },
                    { type: FETCH_MEALS_SUCCESS, items: [
                        {id: 1, name: "Test 1", consumed: new Date("2001-12-04T00:00:00+0000"), calories: 10},
                        {id: 2, name: "Test 2", consumed: new Date("2001-12-10T06:00:00+0200"), calories: 20},
                    ] }
                ])
            })
    })

    it('fetches records from the server and dispatches LOGOUT when fails because token has expired', () => {
        const store = mockStore({ meals: [] })

        fetchMock.getOnce('/api/meals?', {
            status: 401,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(fetchMeals({
            dateFrom: null,
            dateTo: null,
            timeFrom: null,
            timeTo: null,
        }, null))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: FETCH_MEALS_REQUEST },
                    { type: LOGOUT }
                ])
            })
    })

    it('fetches records from the server and dispatches SHOW_ERROR when fails', () => {
        const store = mockStore({ meals: [] })

        fetchMock.getOnce('/api/meals?', {
            status: 500,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(fetchMeals({
            dateFrom: null,
            dateTo: null,
            timeFrom: null,
            timeTo: null,
        }, null))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: FETCH_MEALS_REQUEST },
                    { type: SHOW_ERROR, error: "Internal Server Error"}
                ])
            })
    })

    it('fetches records from the server and uses server date and time format for the filter', () => {
        const store = mockStore({ meals: [] })

        fetchMock.getOnce('/api/meals?dateFrom=2017-12-30&timeFrom=23%3A59%3A59%2B0200', {
            body: [],
            headers: { 'content-type': 'application/json' }
        })

        return store.dispatch(fetchMeals({
            dateFrom: new Date('2017-12-30'),
            dateTo: null,
            timeFrom: new Date('2017-12-30T23:59:59+0200'),
            timeTo: null,
        }, null))
    })

    it('fetches records from the server and uses special endpoint when fetching meals of specified user', () => {
        const store = mockStore({ meals: [] })

        fetchMock.getOnce('/api/meals/user/1?', {
            body: [],
            headers: { 'content-type': 'application/json' }
        })

        return store.dispatch(fetchMeals({
            dateFrom: null,
            dateTo: null,
            timeFrom: null,
            timeTo: null,
        }, 1))
    })

    it('creates a record and dispatches CREATE_MEAL_SUCCESS when succeeds', () => {
        localStorage.__STORE__['token'] = 'TEST_TOKEN'

        fetchMock.postOnce('/api/meals', {
            body: { id: 1, name: "Test", consumed: "2001-12-04T00:00:00+0000", calories: 10 },
            headers: { 'content-type': 'application/json' }
        }, {
            headers: { 'Authorization': 'Bearer TEST_TOKEN', }
        })

        const store = mockStore({ meals: [] })

        return store.dispatch(createMeal("Test", 1, new Date("2001-12-04T00:00:00+0000"), 10)).then(() => {
            expect(fetchMock.lastOptions().body).toEqual(urlEncodeParams({
                name: "Test",
                userId: 1,
                consumed: "2001-12-04T02:00:00+02:00",
                calories: 10,
            }))

            expect(store.getActions()).toEqual([
                { type: CREATE_MEAL_REQUEST, },
                { type: CREATE_MEAL_SUCCESS, id: 1, name: "Test", consumed: new Date("2001-12-04T00:00:00+0000"), calories: 10 },
            ])
        })
    })

    it('creates a record and dispatches CREATE_MEAL_FAILURE when fails (response status 400)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.postOnce('/api/meals', {
            status: 400,
            headers: { 'content-type': 'application/json' },
            body: "\"Name is required\"",
        })

        return store.dispatch(createMeal("Test", 1, new Date("2001-12-04T00:00:00+0000"), 10))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: CREATE_MEAL_REQUEST, },
                    { type: CREATE_MEAL_FAILURE, error: "Name is required" },
                ])
            })
    })

    it('creates a record and dispatches LOGOUT when token has expired', () => {
        const store = mockStore({ meals: [] })

        fetchMock.postOnce('/api/meals', {
            status: 401,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(createMeal("Test", 1, new Date("2001-12-04T00:00:00+0000"), 10))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: CREATE_MEAL_REQUEST, },
                    { type: LOGOUT },
                ])
            })
    })

    it('creates a record and dispatches SHOW_ERROR when fails (response status > 401)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.postOnce('/api/meals', {
            status: 500,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(createMeal("Test", 1, new Date("2001-12-04T00:00:00+0000"), 10))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: CREATE_MEAL_REQUEST, },
                    { type: SHOW_ERROR, error: "Internal Server Error"}
                ])
            })
    })

    it('updates a record and dispatches EDIT_MEAL_SUCCESS when succeeds', () => {
        localStorage.__STORE__['token'] = 'TEST_TOKEN'

        fetchMock.putOnce('/api/meals/1', {
            body: { id: 1, name: "Test", consumed: "2001-12-04T00:00:00+0000", calories: 10 },
            headers: { 'content-type': 'application/json' }
        }, {
            headers: { 'Authorization': 'Bearer TEST_TOKEN', }
        })

        const store = mockStore({ meals: [] })

        return store.dispatch(editMeal(1, "Test", new Date("2001-12-04T00:00:00+0000"), 10)).then(() => {
            expect(fetchMock.lastOptions().body).toEqual(urlEncodeParams({
                name: "Test",
                consumed: "2001-12-04T02:00:00+02:00",
                calories: 10,
            }))

            expect(store.getActions()).toEqual([
                { type: EDIT_MEAL_REQUEST, id: 1 },
                { type: EDIT_MEAL_SUCCESS, id: 1, name: "Test", consumed: new Date("2001-12-04T00:00:00+0000"), calories: 10 },
            ])
        })
    })

    it('updates a record and dispatches LOGOUT when token has expired', () => {
        const store = mockStore({ meals: [] })

        fetchMock.putOnce('/api/meals/1', {
            status: 401,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(editMeal(1, "Test", new Date("2001-12-04T00:00:00+0000"), 10))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: EDIT_MEAL_REQUEST, id: 1 },
                    { type: LOGOUT },
                ])
            })
    })

    it('updates a record and dispatches EDIT_MEAL_FAILURE when fails (response status 400)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.putOnce('/api/meals/1', {
            status: 400,
            headers: { 'content-type': 'application/json' },
            body: "\"Name is required\"",
        })

        return store.dispatch(editMeal(1, "Test", new Date("2001-12-04T00:00:00+0000"), 10))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: EDIT_MEAL_REQUEST, id: 1 },
                    { type: EDIT_MEAL_FAILURE, error: "Name is required" },
                ])
            })
    })

    it('updates a record and dispatches SHOW_ERROR when fails (response status > 401)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.putOnce('/api/meals/1', {
            status: 500,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(editMeal(1, "Test", new Date("2001-12-04T00:00:00+0000"), 10))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: EDIT_MEAL_REQUEST, id: 1 },
                    { type: SHOW_ERROR, error: "Internal Server Error"},
                ])
            })
    })

    it('deletes a record, and dispatches DELETE_MEAL_SUCCESS when succeeds', () => {
        localStorage.__STORE__['token'] = 'TEST_TOKEN'

        fetchMock.deleteOnce('/api/meals/1', { }, {
            headers: { 'Authorization': 'Bearer TEST_TOKEN', }
        })

        const store = mockStore({ meals: [] })

        return store.dispatch(deleteMeal(1)).then(() => {
            expect(store.getActions()).toEqual([
                { type: DELETE_MEAL_REQUEST, id: 1 },
                { type: DELETE_MEAL_SUCCESS, id: 1 },
            ])
        })
    })

    it('deleted a record and dispatches LOGOUT when token has expired', () => {
        const store = mockStore({ meals: [] })

        fetchMock.deleteOnce('/api/meals/1', {
            status: 401,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(deleteMeal(1))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: DELETE_MEAL_REQUEST, id: 1 },
                    { type: LOGOUT, },
                ])
            })
    })

    it('deleted a record and dispatches SHOW_ERROR when fails (response status > 401)', () => {
        const store = mockStore({ meals: [] })

        fetchMock.deleteOnce('/api/meals/1', {
            status: 500,
            headers: { 'content-type': 'application/json' },
            body: "\"\"",
        })

        return store.dispatch(deleteMeal(1))
            .catch(() => {
                expect(store.getActions()).toEqual([
                    { type: DELETE_MEAL_REQUEST, id: 1 },
                    { type: SHOW_ERROR, error: "Internal Server Error" },
                ])
            })
    })
})