import React from 'react'
import PropTypes from 'prop-types'
import RefreshIndicator from 'material-ui/RefreshIndicator'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'

import UsersList from './UsersList'
import UserForm from './UserForm'

class UsersPage extends React.Component {
    constructor(props) {
        super(props)

        this.onUsersFetch = props.onUsersFetch
        this.onUserCreateBegin = props.onUserCreateBegin
        this.onUserCreateCancel = props.onUserCreateCancel
        this.onUserCreateSubmit = props.onUserCreateSubmit

        this.onUserEditBegin = props.onUserEditBegin
        this.onUserEditCancel = props.onUserEditCancel
        this.onUserEditSubmit = props.onUserEditSubmit

        this.onUserDelete = props.onUserDelete
    }

    componentDidMount() {
        this.onUsersFetch()
    }

    render() {
        const changingItem = this.props.users.changingItem
        const userForm = changingItem
            ? <UserForm {...changingItem}
                        auth={this.props.auth}
                        title={changingItem.id ? "Edit user" : "Create user" }
                        isPasswordRequired={!changingItem.id}
                        onSubmit={
                            (id, name, role, password) => changingItem.id
                                ? this.onUserEditSubmit(id, name, role, password)
                                : this.onUserCreateSubmit(name, role, password)
                        }
                        onCancel={
                            changingItem.id
                                ? this.onUserCreateCancel
                                : this.onUserEditCancel
                        }
            /> : null

        return (
            <div>
                { userForm }
                {
                    this.props.users.isFetching
                        ? <div style={{ position: 'relative' }}>
                            <RefreshIndicator
                                size={80}
                                left={-40}
                                top={40}
                                status={'loading'}
                                style={{ marginLeft: '50%' }}
                            />
                        </div>
                        : <UsersList auth={this.props.auth}
                                     users={this.props.users}
                                     onUserEditClick={this.onUserEditBegin}
                                     onUserDeleteClick={this.onUserDelete}
                        />
                }

                <FloatingActionButton style={{
                    margin: 0,
                    top: 'auto',
                    right: 20,
                    bottom: 20,
                    left: 'auto',
                    position: 'fixed',
                }} onClick={this.onUserCreateBegin}>
                    <ContentAdd />
                </FloatingActionButton>
            </div>
        )
    }
}

UsersPage.propTypes = {
    auth: PropTypes.shape({
        user: PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            role: PropTypes.string.isRequired,
        }),
    }),
    users: PropTypes.shape({
        isFetching: PropTypes.bool.isRequired,
        items: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                role: PropTypes.string.isRequired,
            }).isRequired
        ),
        changingItem: PropTypes.shape({
            id: PropTypes.number,
            name: PropTypes.string.isRequired,
            role: PropTypes.string.isRequired,
            isWaiting: PropTypes.bool,
            error: PropTypes.string,
        })
    }),
    onUsersFetch: PropTypes.func.isRequired,

    onUserCreateBegin: PropTypes.func.isRequired,
    onUserCreateCancel: PropTypes.func.isRequired,
    onUserCreateSubmit: PropTypes.func.isRequired,

    onUserEditBegin: PropTypes.func.isRequired,
    onUserEditCancel: PropTypes.func.isRequired,
    onUserEditSubmit: PropTypes.func.isRequired,

    onUserDelete: PropTypes.func.isRequired,
}

export default UsersPage