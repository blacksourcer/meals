<?php

namespace Calories\Tests\Unit\Components;

use Calories\Components\Authenticator;

use Calories\Tests\TestCase;
use Firebase\JWT\JWT;
use Phalcon\Config;
use Phalcon\Http\RequestInterface;

/**
 * Class AuthenticatorTest
 *
 * @package Calories\Tests\Unit\Components
 */
class AuthenticatorTest extends TestCase
{
    /**
     * It parses JWT token from request header
     */
    public function testAuthentication()
    {
        /**
         * @var Config $config
         */
        $config = $this->getConfig();
        $token = JWT::encode([
            'id' => 1,
            'name' => "user",
            'role' => "USER",
            'exp' => time() + 10,
        ], $config->get('jwt')->get('secret'));

        $requestMock = $this->createMock(RequestInterface::class);
        $requestMock->method('getHeader')
            ->willReturnMap([
                ['Authorization', "Bearer $token"]
            ]);

        $authenticator = new Authenticator();
        $authenticator->request = $requestMock;
        $authenticator->config = $config;

        $this->assertNotNull($identity = $authenticator->getCurrentIdentity());
        $this->assertEquals(1, $identity->getId());
        $this->assertEquals("user", $identity->getName());
        $this->assertEquals("USER", $identity->getRole());
    }
}
