<?php

namespace Calories\Components;

use Calories\Components\Authenticator\Identity;

use Firebase\JWT\JWT;
use Phalcon\Config;
use Phalcon\Mvc\User\Component;

/**
 * Class Authenticator
 *
 *
 * @property Config $config
 * @package Calories\Components
 */
class Authenticator extends Component
{
    /**
     * @return Identity|null
     */
    public function getCurrentIdentity(): ?Identity
    {
        $authorization = $this->request->getHeader('Authorization');

        if ($authorization && preg_match("/Bearer\s(.+)/", $authorization, $matches)) {
            foreach ($this->config->get('jwt')->get('magicTokens') as $token => $identityData) {
                if ($matches[1] == $token) {
                    return new Identity(
                        $identityData['id'],
                        $identityData['name'],
                        $identityData['role']
                    );
                }
            }

            try {
                $secret = $this->config->get('jwt')->get('secret');
                $payload = JWT::decode($matches[1], $secret, ['HS256']);
            } catch (\DomainException $ex) {
                return null;
            } catch (\UnexpectedValueException $ex) {
                return null;
            }

            if (!isset($payload->id)
                || !isset($payload->name)
                || !isset($payload->role)
            ) {
                return null;
            }

            return new Identity($payload->id, $payload->name, $payload->role);
        }

        return null;
    }

    /**
     * @param Identity $identity
     * @return string
     */
    public function createToken(Identity $identity): string
    {
        return JWT::encode([
            'id' => $identity->getId(),
            'name' => $identity->getName(),
            'role' => $identity->getRole(),
            'exp' => time() + $this->config->get('jwt')->get('exp'),
        ], $this->config->get('jwt')->get('secret'));
    }
}