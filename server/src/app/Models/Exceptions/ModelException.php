<?php

namespace Calories\Models\Exceptions;

/**
 * Class ModelException
 *
 * @package Calories\Models\Exceptions
 */
class ModelException extends \ErrorException
{

}
