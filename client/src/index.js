import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import reducer from './reducers'
import App from './containers'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

let store = configureStore(reducer)

render(
    <Provider store={store}>
        <MuiThemeProvider>
            <App />
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('root')
)