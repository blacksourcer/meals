import {
    SHOW_LOGIN_DIALOG,
    CLOSE_LOGIN_DIALOG,
    SHOW_SETTINGS_DIALOG,
    CLOSE_SETTINGS_DIALOG,
    SHOW_ERROR,
    DISMISS_ERROR,
} from '../actions/app'

const initialState = {
    showLogin: false,
    showSettings: false,
    error: null,
}

const app = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_LOGIN_DIALOG:
            return {...state, showLogin: true}
        case CLOSE_LOGIN_DIALOG:
            return {...state, showLogin: false}
        case SHOW_SETTINGS_DIALOG:
            return {...state, showSettings: true}
        case CLOSE_SETTINGS_DIALOG:
            return {...state, showSettings: false}
        case SHOW_ERROR:
            return {...state, error: action.error}
        case DISMISS_ERROR:
            return {...state, error: null}
        default:
            return state
    }
}

export default app