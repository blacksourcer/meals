<?php

try {
    require_once __DIR__ . "/bootstrap.php";

    /**
     * @var \Phalcon\DiInterface $di
     */
    $di = require_once "di.php";

    /**
     * @var \Phalcon\Mvc\Micro $app
     */
    $app = require_once "app.php";

    $app->handle();
} catch (Error $ex) {
    /**
     * @var \Phalcon\Http\ResponseInterface $response
     */
    $response = $di->get('response');

    $response
        ->setStatusCode(500, "Server Error")
        ->setContent("Unexpected error occurred")
        ->send();
}

