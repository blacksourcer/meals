<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

use \Phalcon\Config;
use Phalcon\Di\FactoryDefault;

$di = new FactoryDefault();

$di->setShared('config', function () use ($di) {
    $config = new Config(
        require CONFIG_DIR . '/main.php'
    );

    if (APP_ENV === APP_ENV_DEV) {
        $config->merge(new Config(
            require CONFIG_DIR . '/dev.php'
        ));
    }

    return $config;
});

$di->setShared('entityManager', function () use ($di) {
    /**
     * @var Config $config
     */
    $config = $di->get('config');

    $entityManager = EntityManager::create([
        'user' => $config->get('db')->get('user'),
        'password' => $config->get('db')->get('password'),
        'host' => $config->get('db')->get('host'),
        'dbname' => $config->get('db')->get('dbname'),
        'driver' => 'pdo_mysql',
    ], Setup::createAnnotationMetadataConfiguration([
        APP_DIR . '/Models'
    ], APP_ENV === APP_ENV_DEV));

    return $entityManager;
});

$di->setShared('authenticator', function () {
    return new Calories\Components\Authenticator();
});

return $di;