import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import DatePicker from 'material-ui/DatePicker'
import TimePicker from 'material-ui/TimePicker'
import FlatButton from 'material-ui/FlatButton'
import RefreshIndicator from 'material-ui/RefreshIndicator'

class MealForm extends React.Component {

    constructor(props) {
        super(props)

        this.onSubmit = props.onSubmit
        this.onCancel = props.onCancel
        this.state = {
            id: props.id,
            name: props.name,
            consumedDate: props.consumed ? new Date(
                props.consumed.getFullYear(),
                props.consumed.getMonth(),
                props.consumed.getDate()
            ) : null,
            consumedTime: props.consumed,
            calories: props.calories,
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
    }

    handleChange(name, value) {
        this.setState({
            [name]: value
        })
    }

    handleSubmit(event) {
        event.preventDefault()

        let consumed = this.state.consumedDate

        consumed.setHours(this.state.consumedTime.getHours())
        consumed.setMinutes(this.state.consumedTime.getMinutes())

        this.onSubmit(
            this.state.id,
            this.state.name,
            consumed,
            this.state.calories
        )
    }

    handleCancel() {
        this.onCancel()
    }

    render() {
        return (
            <div>
                <Dialog
                    title={this.props.title || "Manage meal record"}
                    actions={[
                        <FlatButton
                            label="Cancel"
                            onClick={this.handleCancel}
                            disabled={this.props.isWaiting}
                        />,
                        <FlatButton
                            label="Ok"
                            primary={true}
                            onClick={this.handleSubmit}
                            disabled={this.props.isWaiting}
                        />
                    ]}
                    modal={true}
                    open={true}
                    onRequestClose={this.handleCancel}
                >
                    {this.props.error &&
                        <p style={{color: 'red'}}>{this.props.error}</p>
                    }
                    {this.props.isWaiting &&
                        <RefreshIndicator size={40}
                                          left={-20}
                                          top={100}
                                          status={'loading'}
                                          style={{ marginLeft: '50%' }}
                        />
                    }
                    <TextField
                        floatingLabelText="Meal name"
                        name="name"
                        value={this.state.name}
                        onChange={(e, value) => this.handleChange("name", value)}
                        disabled={this.props.isWaiting}
                    />
                    <DatePicker
                        floatingLabelText="Meal consumed"
                        name="consumedDate"
                        value={this.state.consumedDate}
                        onChange={(e, value) => this.handleChange("consumedDate", value)}
                        formatDate={date => date.toLocaleDateString()}
                        autoOk={true}
                        disabled={this.props.isWaiting}
                    />
                    <TimePicker
                        name="consumedTime"
                        value={this.state.consumedTime}
                        onChange={(e, value) => this.handleChange("consumedTime", value)}
                        format="24hr"
                        autoOk={true}
                        disabled={this.props.isWaiting}
                    />
                    <TextField
                        floatingLabelText="Calories"
                        name="calories"
                        value={this.state.calories}
                        onChange={(e, value) => this.handleChange("calories", value)}
                        disabled={this.props.isWaiting}
                    />
                </Dialog>
            </div>
        )
    }
}

MealForm.propTypes = {
    id: PropTypes.number,
    name: PropTypes.string,
    consumed: PropTypes.instanceOf(Date),
    calories: PropTypes.number,
    isWaiting: PropTypes.bool,
    error: PropTypes.string,
    title: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
}

MealForm.defaultProps = {
    name: '',
    consumed: new Date(),
    calories: 0
}

export default MealForm