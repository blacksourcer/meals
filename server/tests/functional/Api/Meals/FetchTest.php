<?php

namespace Calories\Tests\Functional\Api\Meals;

use Calories\Models\User;
use Calories\Tests\Functional\Api\MealsTest;

/**
 * Class FetchTest
 *
 * @package Calories\Tests\Functional\Api\Meals
 */
class FetchTest extends MealsTest
{
    /**
     * When unsupported 'Accept' MIME requested it yields HTTP 415 Unsupported Media Type
     */
    public function testUnsupportedAcceptHeader()
    {
        $response = $this->curl->get("{$this->baseUri}/api/meals", [], [
            'Accept: application/xml'
        ]);

        $this->assertEquals(415, $response->header->statusCode);
    }

    /**
     * When no token passed in it yields HTTP 401 Unauthorized
     */
    public function testUnauthorized()
    {
        $response = $this->curl->get("{$this->baseUri}/api/meals");

        $this->assertEquals(401, $response->header->statusCode);
    }

    /**
     * It yields HTTP 403 Forbidden when user tries to access other user's meals list
     */
    public function testForbidden()
    {
        list(, $token) = $this->loginUser();

        list($statusCode) = $this->get("{$this->baseUri}/api/meals/user/0", $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It shows meals list of current user
     */
    public function testGet()
    {
        list($userId1, $token) = $this->loginUser();

        $id1 = $this->createMeal([
            'name' => "Cereal",
            'consumed' => "2001-12-03T09:00:00Z",
            'calories' => 300
        ], $token);

        $id2 = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T13:00:00Z",
            'calories' => 1000
        ], $token);

        list($statusCode, $headers, $body) = $this->get("{$this->baseUri}/api/meals", $token);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        $this->assertEquals([
            [
                'id' => $id1,
                'userId' => $userId1,
                'name' => "Cereal",
                'consumed' => "2001-12-03T09:00:00+0000",
                'calories' => 300
            ],
            [
                'id' => $id2,
                'userId' => $userId1,
                'name' => "Hamburger",
                'consumed' => "2001-12-03T13:00:00+0000",
                'calories' => 1000
            ],
        ], json_decode($body, true));

        list($userId2, $token) = $this->loginUser();

        $id3 = $this->createMeal([
            'name' => "Fries",
            'consumed' => "2001-12-03T13:30:00Z",
            'calories' => 200
        ], $token);

        $id4 = $this->createMeal([
            'name' => "Fruit salad",
            'consumed' => "2001-12-03T19:00:00Z",
            'calories' => 100
        ], $token);

        list($statusCode, $headers, $body) = $this->get("{$this->baseUri}/api/meals", $token);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        $this->assertEquals([
            [
                'id' => $id3,
                'userId' => $userId2,
                'name' => "Fries",
                'consumed' => "2001-12-03T13:30:00+0000",
                'calories' => 200
            ],
            [
                'id' => $id4,
                'userId' => $userId2,
                'name' => "Fruit salad",
                'consumed' => "2001-12-03T19:00:00+0000",
                'calories' => 100
            ],
        ], json_decode($body, true));
    }

    /**
     * It shows meals list by user id for non-admin users if id matches current user
     */
    public function testGetByUserId()
    {
        list($userId, $token) = $this->loginUser();

        $id1 = $this->createMeal([
            'name' => "Cereal",
            'consumed' => "2001-12-03T09:00:00Z",
            'calories' => 300
        ], $token);

        $id2 = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T13:00:00Z",
            'calories' => 1000
        ], $token);

        list($statusCode, $headers, $body) =
            $this->get("{$this->baseUri}/api/meals/user/$userId", $token);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        $this->assertEquals([
            [
                'id' => $id1,
                'userId' => $userId,
                'name' => "Cereal",
                'consumed' => "2001-12-03T09:00:00+0000",
                'calories' => 300
            ],
            [
                'id' => $id2,
                'userId' => $userId,
                'name' => "Hamburger",
                'consumed' => "2001-12-03T13:00:00+0000",
                'calories' => 1000
            ],
        ], json_decode($body, true));
    }

    /**
     * It filters record by date and/or time intervals and orders them from earliest to latest
     */
    public function testGetByDateTime()
    {
        list(, $token) = $this->loginUser();

        $id1 = $this->createMeal([
            'name' => "Cereal",
            'consumed' => "2001-12-03T09:00:00Z",
            'calories' => 300
        ], $token);

        $id2 = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T13:00:00Z",
            'calories' => 1000
        ], $token);

        $id3 = $this->createMeal([
            'name' => "Fries",
            'consumed' => "2001-12-03T13:30:00Z",
            'calories' => 200
        ], $token);

        $id4 = $this->createMeal([
            'name' => "Fruit salad",
            'consumed' => "2001-12-03T19:00:00Z",
            'calories' => 100
        ], $token);

        $id5 = $this->createMeal([
            'name' => "Cheesburger",
            'consumed' => "2001-12-04T13:00:00Z",
            'calories' => 1000
        ], $token);

        $id6 = $this->createMeal([
            'name' => "Borsch",
            'consumed' => "2001-12-04T19:00:00Z",
            'calories' => 800
        ], $token);

        $id7 = $this->createMeal([
            'name' => "Eggs",
            'consumed' => "2001-12-05T09:00:00Z",
            'calories' => 250
        ], $token);

        $toId = function (array $data) {
            return $data['id'];
        };

        list(, , $body) = $this->get("{$this->baseUri}/api/meals", $token, [
            'dateFrom' => "2001-12-03",
        ]);

        $this->assertEquals([
            $id1, $id2, $id3, $id4, $id5, $id6, $id7
        ], array_map($toId, json_decode($body, true)));

        list(, , $body) = $this->get("{$this->baseUri}/api/meals", $token, [
            'dateFrom' => "2001-12-03",
            'dateTo' => "2001-12-04",
        ]);

        $this->assertEquals([
            $id1, $id2, $id3, $id4, $id5, $id6,
        ], array_map($toId, json_decode($body, true)));

        list(, , $body) = $this->get("{$this->baseUri}/api/meals", $token, [
            'dateTo' => "2001-12-04",
        ]);

        $this->assertEquals([
            $id1, $id2, $id3, $id4, $id5, $id6,
        ], array_map($toId, json_decode($body, true)));

        list(, , $body) = $this->get("{$this->baseUri}/api/meals", $token, [
            'dateFrom' => "2001-12-03",
            'timeFrom' => "11:30:00+0200",
        ]);

        $this->assertEquals([
            $id2, $id3, $id4, $id5, $id6,
        ], array_map($toId, json_decode($body, true)));

        list(, , $body) = $this->get("{$this->baseUri}/api/meals", $token, [
            'dateFrom' => "2001-12-03",
            'timeFrom' => "15:00:00+0200",
            'timeTo' => "14:00:00Z",
        ]);

        $this->assertEquals([
            $id2, $id3, $id5,
        ], array_map($toId, json_decode($body, true)));

        list(, , $body) = $this->get("{$this->baseUri}/api/meals", $token, [
            'dateFrom' => "2001-12-03",
            'timeFrom' => "13:00:00Z",
            'dateTo' => "2001-12-03",
            'timeTo' => "19:00:00+0100",
        ]);

        $this->assertEquals([
            $id2, $id3,
        ], array_map($toId, json_decode($body, true)));

        list(, , $body) = $this->get("{$this->baseUri}/api/meals", $token, [
            'dateFrom' => "2001-12-04",
            'timeTo' => "11:00:00+0200",
        ]);

        $this->assertEquals([
            $id7,
        ], array_map($toId, json_decode($body, true)));

        list(, , $body) = $this->get("{$this->baseUri}/api/meals", $token, [
            'dateFrom' => "2001-12-10",
        ]);

        $this->assertEquals([], array_map($toId, json_decode($body, true)));
    }

    /**
     * It shows meals list of any user for ADMIN
     */
    public function testGetAdmin()
    {
        list($userId, $token) = $this->loginUser();

        $id1 = $this->createMeal([
            'name' => "Cereal",
            'consumed' => "2001-12-03T09:00:00Z",
            'calories' => 300
        ], $token);

        $id2 = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-03T13:00:00Z",
            'calories' => 1000
        ], $token);

        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        list(, , $body) = $this->get("{$this->baseUri}/api/meals/user/$userId", $token);

        $this->assertEquals([
            [
                'id' => $id1,
                'userId' => $userId,
                'name' => "Cereal",
                'consumed' => "2001-12-03T09:00:00+0000",
                'calories' => 300
            ],
            [
                'id' => $id2,
                'userId' => $userId,
                'name' => "Hamburger",
                'consumed' => "2001-12-03T13:00:00+0000",
                'calories' => 1000
            ],
        ], json_decode($body, true));
    }

    /**
     * It yields HTTP 403 Forbidden when user tries to access other user's meal
     */
    public function testViewForbidden()
    {
        list(, $token) = $this->loginUser();

        $mealId = $this->createMeal([
            'name' => "Hamburger",
            'calories' => 500,
        ], $token);

        list(, $token) = $this->loginUser();

        list($statusCode) = $this->get("{$this->baseUri}/api/meals/$mealId", $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It shows the meal
     */
    public function testView()
    {
        list($userId, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-31T23:59:59Z",
            'calories' => 500,
        ], $token);

        list(
            $statusCode,
            $headers,
            $body
            ) = $this->get("{$this->baseUri}/api/meals/$id", $token);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        $this->assertEquals([
            'id' => $id,
            'userId' => $userId,
            'name' => "Hamburger",
            'consumed' => "2001-12-31T23:59:59+0000",
            'calories' => 500,
        ], json_decode($body, true), '', 2);
    }

    /**
     * It shows the meal
     */
    public function testViewAdmin()
    {
        list($userId, $token) = $this->loginUser();

        $id = $this->createMeal([
            'name' => "Hamburger",
            'consumed' => "2001-12-31T23:59:59Z",
            'calories' => 500,
        ], $token);

        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        list(, , $body) = $this->get("{$this->baseUri}/api/meals/$id", $token);

        $this->assertEquals([
            'id' => $id,
            'userId' => $userId,
            'name' => "Hamburger",
            'consumed' => "2001-12-31T23:59:59+0000",
            'calories' => 500,
        ], json_decode($body, true), '', 2);
    }
}