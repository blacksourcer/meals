<?php

define("ROOT_DIR", __DIR__);
define("APP_DIR", ROOT_DIR . "/app");
define("DOCTRINE_DIR", ROOT_DIR . "/doctrine");
define("CONFIG_DIR", ROOT_DIR . "/config");
define("VENDOR_DIR", ROOT_DIR . "/../vendor");

define("APP_ENV_PROD", "PRODUCTION");
define("APP_ENV_DEV", "DEVELOPMENT");

if (in_array($env = getenv("APP_ENV"), [
    APP_ENV_PROD,
    APP_ENV_DEV
])) {
    define("APP_ENV", $env);
} else {
    define("APP_ENV", APP_ENV_PROD);
}

define('DATE_FORMAT', "Y-m-d");
define('INT_MAX', 2147483647);

define('MINUTE_BY_SECONDS', 60);
define('HOUR_BY_SECONDS', 60 * MINUTE_BY_SECONDS);
define('DAY_BY_SECONDS', 24 * HOUR_BY_SECONDS);

define('TOKEN_TESTING', 'dev_test');