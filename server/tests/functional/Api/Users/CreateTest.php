<?php

namespace Calories\Tests\Functional\Api\Users;

use Calories\Models\User;
use Calories\Tests\Functional\ApiTest;

/**
 * Class CreateTest
 *
 * @package Calories\Tests\Functional\Api\Users
 */
class CreateTest extends ApiTest
{
    /**
     * When unsupported 'Accept' MIME requested it yields HTTP 415 Unsupported Media Type
     */
    public function testUnsupportedAcceptHeader()
    {
        $response = $this->curl->post("{$this->baseUri}/api/users", [
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], true, [
            'Accept: application/xml'
        ]);

        $this->assertEquals(415, $response->header->statusCode);
    }

    /**
     * When no token passed in it yields HTTP 401 Unauthorized
     */
    public function testUnauthorized()
    {
        $response = $this->curl->post("{$this->baseUri}/api/users", [
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ]);

        $this->assertEquals(401, $response->header->statusCode);
    }

    /**
     * When access level is insufficient it yields HTTP 403 Forbidden
     */
    public function testForbidden()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_USER]);

        list($statusCode) = $this->post("{$this->baseUri}/api/users", [
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It requires admin role to create user with role higher then ROLE_USER
     */
    public function testRoleForbidden()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode) = $this->post("{$this->baseUri}/api/users", [
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
            'role' => User::ROLE_MANAGER,
        ], $token);

        $this->assertEquals(403, $statusCode);
    }

    /**
     * It creates record with just name and password provided
     */
    public function testCreate()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list(
            $statusCode,
            $headers,
            $body
        ) = $this->post("{$this->baseUri}/api/users", [
            'name' => $userName =  "User_" . microtime(true),
            'password' => "qweqwe123",
        ], $token);

        $this->assertEquals(201, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        $user = json_decode($body, true);

        $this->assertEquals("{$this->baseUri}/api/users/{$user['id']}", $headers['Location']);

        $this->assertGreaterThan(0, $user['id']);
        $this->assertEquals($userName, $user['name']);
        $this->assertEquals(User::ROLE_USER, $user['role']);

        $this->userIdList[] = $user['id'];
    }

    /**
     * It allows admin to create ROLE_MANAGER users
     */
    public function testCreateManager()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/users", [
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
            'role' => User::ROLE_MANAGER,
        ], $token);

        $user = json_decode($body, true);

        $this->assertEquals(201, $statusCode);

        $this->userIdList[] = $user['id'];
    }

    /**
     * It allows admin to create ROLE_ADMIN users
     */
    public function testCreateAdmin()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/users", [
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
            'role' => User::ROLE_ADMIN,
        ], $token);

        $user = json_decode($body, true);

        $this->assertEquals(201, $statusCode);

        $this->userIdList[] = $user['id'];
    }

    /**
     * It requires Name parameter to be passed
     */
    public function testCreateNoName()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/users", [
            'password' => "qweqwe123",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Name\" can not be null", json_decode($body));
    }

    /**
     * It requires Name parameter to be not empty
     */
    public function testCreateEmptyName()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/users", [
            'name' => '',
            'password' => "qweqwe123",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Name\" can not be empty", json_decode($body));
    }

    /**
     * It requires Name parameter to be shorter than 256 characters
     */
    public function testCreateNameTooLong()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/users", [
            'name' => "
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
            ",
            'password' => "qweqwe123",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals(
            "\"Name\" can not contain more than " . User::NAME_MAX_LENGTH . " characters",
            json_decode($body)
        );
    }

    /**
     * It doesn't allow to create users with same names
     */
    public function testCreateDuplicateName()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        $this->createUser([
            'name' => $userName = "User_" . microtime(true)
        ], $token);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/users", [
            'name' => $userName,
            'password' => "qweqwe123",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Name\" is already taken", json_decode($body));
    }

    /**
     * It requires Password parameter to be not empty when it is passed
     */
    public function testPasswordEmpty()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/users", [
            'name' => "User_" . microtime(true),
            'password' => "",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Password\" can not be empty", json_decode($body));
    }

    /**
     * It requires Password parameter not to be shorter than 8 characters
     */
    public function testPasswordTooShort()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/users", [
            'name' => "User_" . microtime(true),
            'password' => "12",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals(
            "\"Password\" should contain from " . User::PASSWORD_MIN_LENGTH . " to " . User::PASSWORD_MAX_LENGTH . " characters",
            json_decode($body)
        );
    }

    /**
     * It requires Password parameter to be not longer than 64 characters
     */
    public function testPasswordTooLong()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_MANAGER]);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/users", [
            'name' => "User_" . microtime(true),
            'password' => "
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            ",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals(
            "\"Password\" should contain from " . User::PASSWORD_MIN_LENGTH . " to " . User::PASSWORD_MAX_LENGTH . " characters",
            json_decode($body)
        );
    }

    /**
     * It requires role to be of (ROLE_USER, ROLE_MANAGER, ROLE_ADMIN)
     */
    public function testRoleInvalid()
    {
        list(, $token) = $this->loginUser(['role' => User::ROLE_ADMIN]);

        list($statusCode, , $body) = $this->post("{$this->baseUri}/api/users", [
            'name' => "User_" . microtime(true),
            'password' => "qweqwe123",
            'role' => "gibberish",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Role\" is invalid", json_decode($body));
    }
}