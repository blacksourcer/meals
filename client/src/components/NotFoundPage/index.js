import React from 'react'
import { Card, CardTitle, CardText } from 'material-ui/Card'

const NotFoundPage = () => (
    <Card style={{marginTop: "20px"}}>
        <CardTitle>404 Not Found</CardTitle>
        <CardText>The page is not found</CardText>
    </Card>
)

export default NotFoundPage