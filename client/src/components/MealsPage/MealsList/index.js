import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { List } from 'material-ui/List'
import { Card, CardTitle, CardText } from 'material-ui/Card'

import Meal from '../Meal'
import { ESTIMATED_CALORIES } from '../../../actions/settings'

const MealsList = ({ userId, meals, settings, onMealEditClick, onMealDeleteClick }) => {
    const day = date => moment(date).startOf('day').toDate()
    const seconds = date => moment(date).diff(moment(date).startOf('day'), 'seconds')

    const items = meals.items
        .filter(item => (!meals.filter.dateFrom || day(item.consumed) >= meals.filter.dateFrom)
            && (!meals.filter.dateTo || day(item.consumed) <= meals.filter.dateTo)
            && (!meals.filter.timeFrom || seconds(item.consumed) >= seconds(meals.filter.timeFrom))
            && (!meals.filter.timeTo || seconds(item.consumed) <= seconds(meals.filter.timeTo))
        )
        .sort(
            (item1, item2) => item1.consumed - item2.consumed
        )

    const mealsByDate = items.reduce((groups, item) => {
        let dateString = item.consumed.toLocaleDateString()
        groups[dateString] = groups[dateString] || {items: [], calories: 0}
        groups[dateString].items.push(item)
        groups[dateString].calories += item.calories

        return groups
    }, {})

    const estimatedCalories = settings.items[ESTIMATED_CALORIES]

    const showEstimate = settings.items[ESTIMATED_CALORIES] && !userId

    const totalCaloriesTextStyle = calories =>
        showEstimate
            ? (calories > settings.items[ESTIMATED_CALORIES]
                ? { color: 'red' }
                : { color: 'green' })
            : {}

    return (
        <div>
            {Object.keys(mealsByDate).map(dateString => (
                <Card key={dateString} style={{marginTop: "20px"}}>
                    <CardTitle title={"Meals of the " + dateString} />
                    <CardText>
                        <List>
                            {mealsByDate[dateString].items.map(meal => (
                                <Meal key={meal.id} {...meal}
                                      onEditClick = {onMealEditClick}
                                      onDeleteClick = {onMealDeleteClick}/>
                            ))}
                        </List>
                    </CardText>
                    <CardText style={totalCaloriesTextStyle(mealsByDate[dateString].calories)}>
                        {showEstimate
                            ? "Total calories: " + mealsByDate[dateString].calories + " cal. (of expected "  +estimatedCalories + " cal.)"
                            : "Total calories: " + mealsByDate[dateString].calories + " cal."
                        }
                    </CardText>
                </Card>
            ))}
        </div>
    )
}

MealsList.propTypes = {
    userId: PropTypes.string,
    settings: PropTypes.shape({
        items: PropTypes.shape({
            [ESTIMATED_CALORIES]: PropTypes.string,
        }).isRequired,
    }),
    meals: PropTypes.shape({
        isFetching: PropTypes.bool.isRequired,
        items: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                consumed: PropTypes.instanceOf(Date).isRequired,
                calories: PropTypes.number.isRequired,
                isEditing: PropTypes.bool,
                isWaiting: PropTypes.bool,
            }).isRequired
        ),
        filter: PropTypes.shape({
            dateFrom: PropTypes.instanceOf(Date),
            dateTo: PropTypes.instanceOf(Date),
            timeFrom: PropTypes.instanceOf(Date),
            timeTo: PropTypes.instanceOf(Date),
        })
    }),
    onMealEditClick: PropTypes.func.isRequired,
    onMealDeleteClick: PropTypes.func.isRequired
}

export default MealsList