<?php

namespace Calories\Models;

use Calories\Models\Exceptions\ModelException;

/**
 * Class Setting
 *
 * @package Calories\Models
 *
 * @Entity
 * @Table(name="setting")
 * @HasLifecycleCallbacks
 */
class Setting
{
    const ESTIMATED_CALORIES = 'ESTIMATED_CALORIES';

    const VALUE_MAX_LENGTH = 256;

    /**
     * @var User
     *
     * @Id
     * @ManyToOne(targetEntity="Calories\Models\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;

    /**
     * @var string
     *
     * @Id
     * @Column(type="string", length=32, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", length=256, nullable=false)
     */
    protected $value;

    /**
     * @throws ModelException
     */
    protected function validateName()
    {
        $name = $this->getName();

        if (null === $name) {
            throw new ModelException("\"Name\" can not be null");
        }

        if (!in_array($name, [self::ESTIMATED_CALORIES])) {
            throw new ModelException("\"Name\" is invalid");
        }
    }

    /**
     * @throws ModelException
     */
    protected function validateValue()
    {
        $value = $this->getValue();

        if ($value && strlen($value) > self::VALUE_MAX_LENGTH) {
            throw new ModelException("\"Value\" can not contain more than " . self::VALUE_MAX_LENGTH . " characters");
        }

        switch ($this->getName()) {
            case self::ESTIMATED_CALORIES:
                if ($value && !is_numeric($value)) {
                    throw new ModelException("\"Value\" for " . self::ESTIMATED_CALORIES . " must be numeric");
                }
                break;
        }
    }

    #region Getters and Setters

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return static
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return static
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return static
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    #endregion

    /**
     * @throws ModelException
     *
     * @PrePersist @PreUpdate
     */
    public function validate()
    {
        $this->validateName();
        $this->validateValue();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name' => $this->getName(),
            'value' => $this->getValue(),
        ];
    }
}