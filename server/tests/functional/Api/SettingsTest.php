<?php

namespace Calories\Tests\Functional\Api\Meals;

use Calories\Models\Meal;

use Calories\Models\Setting;
use Calories\Tests\Functional\ApiTest;

/**
 * Class SettingsTest
 *
 * @package Calories\Tests\Functional\Api\Meals
 */
class SettingsTest extends ApiTest
{
    /**
     * When unsupported 'Accept' MIME requested it yields HTTP 415 Unsupported Media Type
     */
    public function testUnsupportedAcceptHeader()
    {
        $response = $this->curl->get("{$this->baseUri}/api/settings", [], [
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/xml',
        ]);

        $this->assertEquals(415, $response->header->statusCode);

        $response = $this->curl->put("{$this->baseUri}/api/settings", [
            'name' => Setting::ESTIMATED_CALORIES,
        ], true, [
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/xml',
        ]);

        $this->assertEquals(415, $response->header->statusCode);
    }

    /**
     * When no token passed in it yields HTTP 401 Unauthorized
     */
    public function testUnauthorized()
    {
        $response = $this->curl->get("{$this->baseUri}/api/settings");

        $this->assertEquals(401, $response->header->statusCode);

        $response = $this->curl->put("{$this->baseUri}/api/settings", [
            'name' => Setting::ESTIMATED_CALORIES,
        ]);

        $this->assertEquals(401, $response->header->statusCode);
    }

    /**
     * It updates the record
     */
    public function testSave()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, $headers) = $this->put("{$this->baseUri}/api/settings", [
            Setting::ESTIMATED_CALORIES => 2000,
        ], $token);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals('application/json; charset=UTF-8', $headers['Content-Type']);

        list(, , $body) = $this->get("{$this->baseUri}/api/settings", $token);

        $this->assertEquals([
            Setting::ESTIMATED_CALORIES => 2000,
        ], json_decode($body, true));
    }

    /**
     * It requires Name parameter to be from predefined list of constants
     */
    public function testSaveInvalidName()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/settings", [
            'gibberish' => 1,
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals("\"Name\" is invalid", json_decode($body));
    }

    /**
     * It requires Value parameter to be shorter than 256 characters
     */
    public function testSaveValueTooLong()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/settings", [
            Setting::ESTIMATED_CALORIES => "
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
                1231231230123123123012312312301231231230123123123012312312301231231230123123123012312312301231231230
            ",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals(
            "\"Value\" can not contain more than " . Meal::NAME_MAX_LENGTH . " characters",
            json_decode($body)
        );
    }

    /**
     * It requires Setting::ESTIMATED_CALORIES to be numeric
     */
    public function testSaveEstimatedCaloriesNonNumeric()
    {
        list(, $token) = $this->loginUser();

        list($statusCode, , $body) = $this->put("{$this->baseUri}/api/settings", [
            Setting::ESTIMATED_CALORIES => "abc",
        ], $token);

        $this->assertEquals(400, $statusCode);
        $this->assertEquals(
            "\"Value\" for " . Setting::ESTIMATED_CALORIES . " must be numeric",
            json_decode($body)
        );
    }
}