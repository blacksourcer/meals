import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import api from '../middleware/api'
const configureStore = (reducer, preloadedState) => createStore(
    reducer,
    preloadedState,
    applyMiddleware(api, thunk)
)

export default configureStore