import React from 'react'
import PropTypes from 'prop-types'
import RefreshIndicator from 'material-ui/RefreshIndicator'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'

import MealsList from './MealsList'
import MealForm from './MealForm'
import FilterBar from './FilterBar'
import { ESTIMATED_CALORIES } from '../../actions/settings'

class MealsPage extends React.Component {
    constructor(props) {
        super(props)

        this.onSetMealsFilter = props.onSetMealsFilter

        this.onMealsFetch = props.onMealsFetch
        this.onMealCreateBegin = props.onMealCreateBegin
        this.onMealCreateCancel = props.onMealCreateCancel
        this.onMealCreateSubmit = props.onMealCreateSubmit

        this.onMealEditBegin = props.onMealEditBegin
        this.onMealEditCancel = props.onMealEditCancel
        this.onMealEditSubmit = props.onMealEditSubmit

        this.onMealDelete = props.onMealDelete
    }

    componentDidMount() {
        this.onMealsFetch(
            this.props.meals.filter,
            this.props.match.params.userId
        )
    }

    componentWillReceiveProps (newProps) {
        if (this.props.match.params.userId !== newProps.match.params.userId) {
            this.onMealsFetch(
                this.props.meals.filter,
                newProps.match.params.userId
            )
        }
    }

    render() {
        const userId = this.props.match.params.userId

        const changingItem = this.props.meals.changingItem
        const mealForm = changingItem
            ? <MealForm {...changingItem}
                        title={changingItem.id ? "Edit meal" : "Create meal" }
                        onSubmit={
                            (id, name, consumed, calories) => changingItem.id
                                ? this.onMealEditSubmit(id, name, consumed, calories)
                                : this.onMealCreateSubmit(name, userId, consumed, calories)
                        }
                        onCancel={
                            changingItem.id
                                ? this.onMealCreateCancel
                                : this.onMealEditCancel
                        }
            /> : null

        return (
            <div>
                { mealForm }
                <FilterBar filter={this.props.meals.filter}
                           onApply={(filter) => this.onSetMealsFilter(filter, this.props.match.params.userId)}
                           isEnabled={!this.props.meals.isFetching} />
                {
                    this.props.meals.isFetching
                        ? <div style={{ position: 'relative' }}>
                            <RefreshIndicator
                                size={80}
                                left={-40}
                                top={40}
                                status={'loading'}
                                style={{ marginLeft: '50%' }}
                            />
                        </div>
                        : <MealsList meals={this.props.meals}
                                     settings={this.props.settings}
                                     userId={this.props.match.params.userId}
                                     onMealEditClick={this.onMealEditBegin}
                                     onMealDeleteClick={this.onMealDelete}
                        />
                }

                <FloatingActionButton style={{
                    margin: 0,
                    top: 'auto',
                    right: 20,
                    bottom: 20,
                    left: 'auto',
                    position: 'fixed',
                }} onClick={this.onMealCreateBegin}>
                    <ContentAdd />
                </FloatingActionButton>
            </div>
        )
    }
}

MealsPage.propTypes = {
    settings: PropTypes.shape({
        items: PropTypes.shape({
            [ESTIMATED_CALORIES]: PropTypes.string,
        }).isRequired,
    }),
    meals: PropTypes.shape({
        isFetching: PropTypes.bool.isRequired,
        items: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                consumed: PropTypes.instanceOf(Date).isRequired,
                calories: PropTypes.number.isRequired,
            }).isRequired
        ),
        changingItem: PropTypes.shape({
            id: PropTypes.number,
            name: PropTypes.string.isRequired,
            consumed: PropTypes.instanceOf(Date).isRequired,
            calories: PropTypes.number.isRequired,
            isWaiting: PropTypes.bool,
            error: PropTypes.string,
        }),
        filter: PropTypes.shape({
            dateFrom: PropTypes.instanceOf(Date),
            dateTo: PropTypes.instanceOf(Date),
            timeFrom: PropTypes.instanceOf(Date),
            timeTo: PropTypes.instanceOf(Date),
        }),
    }),
    match: PropTypes.shape({
        params: PropTypes.shape({
            userId: PropTypes.string
        })
    }),
    onSetMealsFilter: PropTypes.func.isRequired,

    onMealsFetch: PropTypes.func.isRequired,

    onMealCreateBegin: PropTypes.func.isRequired,
    onMealCreateCancel: PropTypes.func.isRequired,
    onMealCreateSubmit: PropTypes.func.isRequired,

    onMealEditBegin: PropTypes.func.isRequired,
    onMealEditCancel: PropTypes.func.isRequired,
    onMealEditSubmit: PropTypes.func.isRequired,

    onMealDelete: PropTypes.func.isRequired,
}

export default MealsPage