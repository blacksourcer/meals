<?php

require_once __DIR__ . "/bootstrap.php";

/**
 * @var \Phalcon\DiInterface $di
 */
$di = require_once "di.php";

/**
 * @var \Doctrine\ORM\EntityManager $entityManager
 */
$entityManager = $di->get('entityManager');

if (3 !== $argc) {
    echo "USAGE: php cli-createAdmin.php <Name> <Password>\n";
}

list(, $name, $password) = $argv;

try {
    $entityManager->persist(
        (new \Calories\Models\User())
            ->setName($name)
            ->setPassword($password)
            ->setRole(\Calories\Models\User::ROLE_ADMIN)
    );

    $entityManager->flush();

    echo "Success !\n";
    exit(0);
} catch (Error $ex) {
    echo $ex;
    exit(1);
}