import { connect } from 'react-redux'
import {
    fetchUsers,
    createUserBegin,
    createUserCancel,
    createUser,
    editUserBegin,
    editUserCancel,
    editUser,
    deleteUser,
    ROLE_USER,
} from '../../actions/users'

import UsersPage from '../../components/UsersPage'

const mapStateToProps = state => ({
    auth: state.auth,
    users: state.users,
    error: state.error,
})

const mapDispatchToProps = dispatch => ({
    onUsersFetch: () => dispatch(fetchUsers()).catch(() => {}),
    onUserCreateBegin: () => dispatch(createUserBegin('', ROLE_USER, '')),
    onUserCreateCancel: () => dispatch(createUserCancel()),
    onUserCreateSubmit: (name, role, password) => dispatch(createUser(name, role, password))
        .catch(() => {}),
    onUserEditBegin: (id) => dispatch(editUserBegin(id)),
    onUserEditCancel: () => dispatch(editUserCancel()),
    onUserEditSubmit: (id, name, role, password) => dispatch(editUser(id, name, role, password))
        .catch(() => {}),
    onUserDelete: (id) => dispatch(deleteUser(id)).catch(() => {}),
})

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage)