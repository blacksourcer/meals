import React from 'react'
import PropTypes from 'prop-types'
import { BrowserRouter as Router, Route, Switch, Redirect, Link } from 'react-router-dom'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'

import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'

import {
    ROLE_MANAGER,
    ROLE_ADMIN
} from '../actions/users'
import { ESTIMATED_CALORIES } from '../actions/settings'

import MealsPage from '../containers/MealsPage'
import UsersPage from '../containers/UsersPage'
import NotFoundPage from '../components/NotFoundPage'

import LoginDialog from './LoginDialog'
import SettingsDialog from './SettingsDialog'
import ErrorDialog from './ErrorDialog'

class App extends React.Component {

    constructor(props) {
        super(props)
    }

    componentWillMount() {
        if (!this.props.auth.user) {
            this.props.onLoginRequired()
        } else {
            this.props.onSettingsRequired()
        }
    }

    componentWillReceiveProps(newProps) {
        if (this.props.auth.user && !newProps.auth.user) {
            this.props.onLoginRequired()
        }
    }

    render() {
        const isUser = this.props.auth.user || false
        const isManager = isUser && this.props.auth.user.role === ROLE_MANAGER
        const isAdmin = isUser && this.props.auth.user.role === ROLE_ADMIN

        return (
            <Router>
                <div>
                    { this.props.app.showLogin &&
                        <LoginDialog {...this.props.auth}
                                     onSubmit={this.props.onLoginSubmit}
                        />
                    }
                    { this.props.app.showSettings &&
                        <SettingsDialog {...this.props.settings}
                                      onSubmit={this.props.onSettingsSubmit}
                                      onCancel={this.props.onSettingsCancel}
                        />
                    }
                    { this.props.app.error &&
                        <ErrorDialog message={this.props.app.error}
                                     onDismiss={this.props.onDismissError}
                        />
                    }
                    <AppBar title="Calories"
                            iconElementRight={isUser &&
                                <IconMenu
                                    iconButtonElement={
                                        <IconButton><MoreVertIcon /></IconButton>
                                    }
                                    targetOrigin={{horizontal: 'right', vertical: 'top'}}
                                    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                                >
                                    <MenuItem primaryText="Meals"
                                              containerElement={<Link to="/meals" />}
                                    />
                                    {(isManager || isAdmin) &&
                                        <MenuItem primaryText="Users"
                                                  containerElement={<Link to="/users" />}
                                        />
                                    }
                                    <MenuItem primaryText="Settings"
                                              onClick={this.props.onSettingsClick}
                                              disabled={this.props.settings.isFetching || this.props.settings.isWaiting}
                                    />
                                    <MenuItem primaryText="Sign out" onClick={this.props.onLogout} />
                                </IconMenu> || null
                            }
                            showMenuIconButton={false}
                    />
                    <Switch>
                        <Route exact path="/" component={
                            () => isUser ? <Redirect to="/meals" /> : null
                        }
                        />
                        { isUser && <Route exact path="/meals" component={MealsPage} /> }
                        { isAdmin && <Route exact path="/meals/user/:userId(\d+)" component={MealsPage} /> }
                        { (isManager || isAdmin) && <Route exact path="/users" component={UsersPage} /> }
                        { isUser
                            ? <Route component={NotFoundPage} />
                            : <Redirect to="/" />
                        }
                    </Switch>
                </div>
            </Router>
        )
    }
}

App.propTypes = {
    app: PropTypes.shape({
        showLogin: PropTypes.bool.isRequired,
        showSettings: PropTypes.bool.isRequired,
        error: PropTypes.string,
    }),
    auth: PropTypes.shape({
        isWaiting: PropTypes.bool.isRequired,
        error: PropTypes.string,
        user: PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            role: PropTypes.string.isRequired,
        }),
    }),
    settings: PropTypes.shape({
        isWaiting: PropTypes.bool,
        error: PropTypes.string,
        items: PropTypes.shape({
            [ESTIMATED_CALORIES]: PropTypes.string,
        }).isRequired,
    }),
    error: PropTypes.string,
    onLoginRequired: PropTypes.func.isRequired,
    onLoginSubmit: PropTypes.func.isRequired,
    onSettingsRequired: PropTypes.func.isRequired,
    onSettingsClick: PropTypes.func.isRequired,
    onSettingsSubmit: PropTypes.func.isRequired,
    onSettingsCancel: PropTypes.func.isRequired,
    onLogout: PropTypes.func.isRequired,
    onDismissError: PropTypes.func.isRequired,
}

export default App