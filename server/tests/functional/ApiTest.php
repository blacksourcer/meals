<?php

namespace Calories\Tests\Functional;

use Phalcon\Config;
use Phalcon\Http\Client\Provider\Curl;

use Calories\Tests\TestCase;

/**
 * Class ApiTest
 *
 * @package Calories\Tests\Functional
 */
abstract class ApiTest extends TestCase
{
    /**
     * @var string
     */
    protected $baseUri;

    /**
     * @var Curl
     */
    protected $curl;

    /**
     * @var int[]
     */
    protected $userIdList = [];

    /**
     * @return array
     */
    private function generateDefaultUserData(): array
    {
        return [
            'name' => "test_" . microtime(true),
            'password' => "test1234"
        ];
    }

    /**
     * Helper function to create a user
     *
     * @param array $userData
     * @param string|null $token
     * @return int
     */
    protected function createUser(array $userData = [], string $token = null)
    {
        $userData = array_merge($this->generateDefaultUserData(), $userData);

        $response = $this->curl->post("{$this->baseUri}/api/users", $userData, true, [
            "Authorization: Bearer $token"
        ]);

        $user = json_decode($response->body, true);

        if (!$user || !isset($user['id'])) {
            $this->fail("Failed to create user record");
        }

        return $this->userIdList[] = $user['id'];
    }

    /**
     * Helper function to create and log user in
     *
     * @param array $userData
     * @return mixed
     */
    protected function loginUser(array $userData = [])
    {
        $userData = array_merge($this->generateDefaultUserData(), $userData);

        $id = $this->createUser($userData, TOKEN_TESTING);

        $response = $this->curl->post("{$this->baseUri}/api/auth/login", [
            'name' => $userData['name'],
            'password' => $userData['password'],
        ]);

        $json = json_decode($response->body, true);

        if (!$json || !isset($json['token'])) {
            $this->fail("Failed to log the user in");
        }

        return [$id, $json['token']];
    }

    /**
     * Helper method for GET requests
     *
     * @param string $uri
     * @param string|null $token
     * @param array $args
     * @return array
     */
    protected function get(string $uri, string $token = null, array $args = [])
    {
        $customHeaders = $token ? [
            "Authorization: Bearer $token"
        ]: [];

        $response = $this->curl->get($uri, $args, $customHeaders);

        return [
            $response->header->statusCode,
            $response->header->getAll(),
            $response->body
        ];
    }

    /**
     * Helper method for POST requests
     *
     * @param string $uri
     * @param array $data
     * @param string|null $token
     * @return array
     */
    protected function post(string $uri, array $data, string $token = null)
    {
        $customHeaders = $token ? [
            "Authorization: Bearer $token"
        ]: [];

        $response = $this->curl->post($uri, $data, true, $customHeaders);

        return [
            $response->header->statusCode,
            $response->header->getAll(),
            $response->body
        ];
    }

    /**
     * Helper method for PUT requests
     *
     * @param string $uri
     * @param array $data
     * @param string|null $token
     * @return array
     */
    protected function put(string $uri, array $data, string $token = null)
    {
        $customHeaders = [
            'Content-Type: application/x-www-form-urlencoded',
        ];

        if ($token) {
            $customHeaders[] = "Authorization: Bearer $token";
        }

        $response = $this->curl->put($uri, $data, true, $customHeaders);

        return [
            $response->header->statusCode,
            $response->header->getAll(),
            $response->body
        ];
    }

    /**
     * Helper method for DELETE requests
     *
     * @param string $uri
     * @param string|null $token
     * @return array
     */
    protected function delete(string $uri, string $token = null)
    {
        $customHeaders = [];

        if ($token) {
            $customHeaders[] = "Authorization: Bearer $token";
        }

        $response = $this->curl->delete($uri, [], $customHeaders);

        return [
            $response->header->statusCode,
            $response->header->getAll(),
            $response->body
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        /**
         * @var Config $config
         */
        $this->baseUri = ($config = $this->getConfig())
            ? $config->get('baseUri')
            : "";

        $this->curl = new Curl();
    }

    public function tearDown()
    {
        parent::tearDown();

        foreach ($this->userIdList as $userId) {
            $this->curl->delete("{$this->baseUri}/api/users/$userId", [], [
                "Authorization: Bearer " . TOKEN_TESTING
            ]);
        }
    }
}